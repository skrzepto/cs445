from src.Categories.HomeCategory import HomeCategory
from src.Customer.AllCustomers import AllCustomers
from src.Sort.Sort import SortListings

class SAD:

    def __init__(self):
        self.home_cat = HomeCategory()
        self.customers = AllCustomers()
        self.settings = 'random'

    def search(self, srch):
        home_search = self.home_cat.depth_first_search_subcategories(str(srch))
        customers_search = self.customers.search_all_customers(str(srch))

        home_search = [l.to_dict() for l in home_search]
        customers_search = [c.to_dict() for c in customers_search]

        result = {'listings': home_search, 'customers': customers_search}

        return result

    def getListingbyPathID(self, path, l_id):
        sub_cat = self.home_cat.traverse(path)
        if sub_cat:
            li = sub_cat.getListingByID(int(l_id))
            return li
        else:
            return None

    def getAdvertiserbyID(self, c_id):
        cust_res = self.customers.getCustomerbyID(int(c_id))
        return cust_res


    def getAllAdvertiser(self):
        res = self.customers.arr_cust_to_dictarr()
        return res

    def addListingbyPath(self, path, listing):
        # check if advertiser is valid
        if self.getAdvertiserbyID(listing.getCustID()):
            c = self.home_cat.traverse(path)
            if c:
                l = c.addListing(listing)
                return l.getListingID()

        return None

    def getListingbyPath(self, path):
        if path == []:
            home_feat = self.home_cat.getActiveHomePageFeatured()
            sub_cat_arr = self.home_cat.getCategoryNames()
            home_feat = self.sort(home_feat)
            result = {'/': {'home-page-featured': home_feat, 'sub-categories': sub_cat_arr}}
            return result

        sub_cat = self.home_cat.traverse(path)
        if sub_cat:

            normal = sub_cat.getActiveNormalListing()
            normal = [l.to_dict() for l in normal]

            feat = self.home_cat.get_cat_featured(path)
            feat = [l.to_dict() for l in feat]

            sub_cat_arr = sub_cat.getCategoryNames()
            feat = self.sort(feat)
            #SORT HERE

            result = {'/'.join(path): {'category-featured': feat, 'normal': normal, 'sub-categories': sub_cat_arr}}
            return result

        else:
            return None


    def getListingByCustomer(self, c_id):
        res = self.home_cat.getListingByCustomer(int(c_id))
        if res:
            return res
        else:
            return []

    def traverse(self, path):
        if path == []:
            return self.home_cat
        elif len(path)>0:
            res = self.home_cat.traverse(path)
            return res
        else:
            return None

    def editSortSetting(self, settings):
        if settings in ["random", "most-profitable", "most-recent", "advertiser-name"]:
            self.settings = settings
            print(self.settings)
            return self.settings
        else:
            return None

    def sort(self, arr):
        s = SortListings()

        if self.settings == 'random':
            return s.random(arr)

        elif self.settings == 'most-profitable':
            return s.profitability(arr)

        elif self.settings == 'most-recent':
            return s.start_date(arr)

        elif self.settings == 'advertiser-name':
            return s.name(arr, self.customers.dict_name_id())
