import shelve


class datashelve_t():
    def __init__(self, env='test'):
        if env in ['test', 'production']:
            self.env = env

    def getData(self):

        file_name = self.env +'_data'

        s = shelve.open(file_name,  writeback=True)
        res = None
        try:
            res = s['data']
        except KeyError:
            print('I cant find the key data in the file')
        finally:
            s.close()
        print(res)
        return res

    def storeData(self, data):

        file_name = self.env +'_data'
        s = shelve.open(file_name,  writeback=True)
        try:
            #if self.env == 'production':
            s['data'] = data
        finally:
            s.close()


