__author__ = 'shims'

from src.Customer.Customer import Customer
from datetime import datetime
class AllCustomers():
    def __init__(self, all_cust_list=[]):
        self.customers = []
        if all_cust_list:
            self.create_customer_list(all_cust_list)

    def create_customer_list(self, all_cust_tuple):
        for cust in all_cust_tuple:
            if len(cust) == 4:
                dt = datetime.strptime(cust[3], '%b %d %Y %I:%M%p')  # eg. 'Jun 10 2015  1:33PM'
                self.customers.append(Customer(cust[1], cust[2], cust[0], dt))
            elif len(cust) == 3:
                self.customers.append(Customer(cust[1], cust[2], cust[0]))


        return self.customers

    def arr_cust_to_dictarr(self):
        arr_cust = self.customers
        result = []
        for c in arr_cust:
            result.append(c.to_dict())
        return result

    def search_all_customers(self, key):
        all_cust = self.customers
        results = []
        for c in all_cust:
            if c.search(key):
                results.append(c)
        print(results)
        return results

    def getmaxid(self):
        max_id = 0
        for cust in self.customers:
            if cust.get_custId() > max_id:
                max_id = cust.get_custId()
        return max_id

    def addCustomer(self, cust):
        nextid = self.getmaxid() +1
        cust.setID(nextid)
        self.customers.append(cust)
        print(cust.get_custId())
        return self.customers[-1]

    def getCustomerbyID(self, c_id):
        for cust in self.customers:
            if c_id == cust.get_custId():
                return cust
        return None

    def getAllCustomers(self):
        return self.customers

    def dict_name_id(self):
        res = []
        for cust in self.customers:
            res.append({'cust_id': cust.get_custId(), 'name': cust.getName()})

        return res