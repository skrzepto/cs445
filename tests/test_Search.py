import sys
sys.path.append('/home/shims/Documents/cs445/')
import unittest

from src.Search.Search import Search

class TestSearch(unittest.TestCase):

    def setUp(self):
        self.s = Search()

    def testStrSearchFound(self):
        self.assertTrue(Search._search("tests", "t"))

    def testStrSearchNotFound(self):
        self.assertFalse(Search._search("Test", "hello"))

if __name__ == '__main__':
    unittest.main()
