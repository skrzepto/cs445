import dateutil

__author__ = 'shims'
import dateutil.relativedelta
from datetime import datetime
from src.Search.Search import Search


class Listing():
    def __init__(self, cid=0, lid=0):
        self.cust_id = cid  # default id is 0 but pass in when creating listing from customer
        self.start_date = datetime.now()  # default start time is when object is created
        self.end_date = self.start_date + dateutil.relativedelta.relativedelta(
            days=7)  # default end time 7 days after creation
        self.content = 'Default Content' # description of whats being adv, the image, website url
        self.listing_id = lid  # this is auto updated
        self.cost = 100  # default price for a 1 week listing
        self.type = 'normal'  # default other options: category-featured, home-page-featured
        self.website = 'somewebsite.com'
        self.image = 'path/to/image'
        self.path = []

    def set_init_date(self, dat):  # this function is only used when initializing the listing
        self.start_date = dat
        self.end_date = self.start_date + dateutil.relativedelta.relativedelta(days=7) #default end date to be a week later
    def updateContent(self, content):
        self.content = content

    def getContent(self):
        return self.content

    def changeStartDate(self, new_date):
        if self.start_date > datetime.now():
            self.start_date = new_date
            return self.start_date
        else:
            print('Can not update date when it has already started')
            return None

    def setEndDate(self, date):
        if self.start_date >= date:
            return None
        else:
            self.end_date = date
            return self.end_date

    def setCost(self, cost):
        if cost >= 0:
            self.cost = cost
            return cost
        else:
            print("Cost must be a positive number")
            return None

    def getCost(self):
        return self.cost

    def getStartDate(self):
        return self.start_date

    def getEndDate(self):
        return self.end_date

    def setListingID(self, l_id):
        self.listing_id = l_id

    def getListingID(self):
        return self.listing_id

    def setImagePath(self, img_path):
        self.image = str(img_path)

    def getImagePath(self):
        return self.image

    def setWebsite(self, web_url):
        self.website = str(web_url)

    def getWebsite(self):
        return self.website

    def getType(self):
        return self.type

    def updateType(self, type):
        type_list = ['normal', 'category-featured', 'home-page-featured']
        if any(str(type) in s for s in type_list):
            self.type = type
        else:
            return None

    def to_dict(self):
        time_d = self.end_date - self.start_date
        profit = self.getCost() / abs(time_d.days)
        return {'cust_id': self.cust_id, 'start_date': self.start_date.strftime('%b %d %Y %I:%M%p'),
                'end_date': self.end_date.strftime('%b %d %Y %I:%M%p'),
                'content': self.content, 'listing_id': self.listing_id,
                'cost': self.cost, 'type': self.type, 'image': self.image,
                'website': self.website, 'path': self.path, 'profit': profit, 'active': self.isActive()}


    def toString(self):
        tmp = str(self.cust_id) + ' ' + str(self.start_date.strftime('%b %d %Y %I:%M%p')) + ' ' + str(self.end_date.strftime('%b %d %Y %I:%M%p')) + ' ' + str(self.content) + ' ' + str(self.listing_id) + ' ' + str(self.cost) + ' ' + str(self.type)
        return tmp

    def search(self, sch):
        return Search._search(self.toString(), str(sch))

    def getCustID(self):
        return self.cust_id

    def isActive(self):
        if self.getStartDate() <= datetime.now() <= self.getEndDate():
            return True
        else:
            return False

    def setPath(self, path):
        self.path = path

    def getPath(self):
        return self.path




