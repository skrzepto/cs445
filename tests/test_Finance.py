#import sys
#sys.path.append('/home/shims/Documents/cs445/')

import unittest2 as unittest
from src.Categories.HomeCategory import HomeCategory
from src.Interactors.Finance import Finance
from src.Listing.Listing import Listing
from datetime import datetime
import dateutil.relativedelta


class testFinance(unittest.TestCase):

    def setUp(self):
        self.home_cat = HomeCategory()
        self.finance = Finance()

    def create3lvl(self):

        d_now = datetime.now()
        d_yesterday = d_now - dateutil.relativedelta.relativedelta(days=1)
        d_lastweek = d_now - dateutil.relativedelta.relativedelta(days = 7)
        d_lastmonth = d_now - dateutil.relativedelta.relativedelta(months = 1)
        d_last2months = d_now - dateutil.relativedelta.relativedelta(months = 2)
        d_last5months = d_now - dateutil.relativedelta.relativedelta(months = 5)



        d_tommorow = d_now + dateutil.relativedelta.relativedelta(days=1)
        d_nextweek = d_now + dateutil.relativedelta.relativedelta(days=7)
        d_nextmonth = d_now + dateutil.relativedelta.relativedelta(months=1)


        l_home = Listing(0, 1)
        l_home.updateType('home-page-featured')
        l_home.updateContent('This is a home page listing')
        l_home.setCost(2500)
        l_home.set_init_date(d_yesterday)
        l_home.setEndDate(d_nextmonth)
        self.home_cat.addListing(l_home)



        l = Listing(0)
        l.set_init_date(d_lastmonth)
        l.setEndDate(d_nextweek)
        l.updateContent('where is waldo in sports: normal')
        l.setCost(300)

        l_1 = Listing(0)
        l_1.set_init_date(d_last5months)
        l_1.setEndDate(d_last2months)
        l_1.updateContent('where is waldo in sports: category featured')
        l_1.updateType('category-featured')
        l_1.setCost(400)



        cat_sports = self.home_cat.addSubCategory('sports')
        cat_sports['sports'].addListing(l)
        cat_sports['sports'].addListing(l_1)


        l2 = Listing(0)
        l2.set_init_date(d_last2months)
        l2.setEndDate(d_nextmonth)
        l2.setCost(250)
        l2.updateContent('waldo is in sports featured')

        cat_soccer = cat_sports['sports'].addSubCategory('soccer')
        cat_soccer['soccer'].addListing(l2)

        l3 = Listing(0)
        l3.set_init_date(d_lastweek)
        l3.setEndDate(d_nextmonth)
        l3.updateType('category-featured')
        l3.updateContent('waldo is in sports featured')

        cat_gear = cat_soccer['soccer'].addSubCategory('gear')
        cat_gear['gear'].addListing(l3)

        print(self.home_cat)

    def tearDown(self):
        self.finance = None
        self.home_cat = None

    def test_income_one_month(self):
        self.create3lvl()
        self.assertEqual(self.finance.getIncomeMonths(1, self.home_cat), 2600)

    def test_income_6_month(self):

        delta = datetime.now() - dateutil.relativedelta.relativedelta(months=int(6))
        print(delta)

        self.create3lvl()
        self.assertEqual(self.finance.getIncomeMonths(6, self.home_cat), 3550)




if __name__ == "main":
    unittest.main()
