import unittest

import sys
sys.path.append('/home/shims/Documents/cs445/')

from src.Listing.Listing import Listing
from collections import Counter

import datetime as dt
import dateutil.relativedelta
from datetime import datetime


class TestListing(unittest.TestCase):

    def setUp(self):
        self.listing = Listing()

    def tearDown(self):
        self.listing = None

    def test_updateContent(self):
        self.listing.updateContent('conten')
        self.assertEqual(self.listing.getContent(), 'conten')

    def test_set_initial_date(self):
        self.listing.set_init_date(datetime.now())
        self.assertAlmostEqual(self.listing.start_date, datetime.now(), delta=dt.timedelta(seconds=60))

    def test_startdate_afterstart(self):
        # modify date so it started yesterday
        d = datetime.now()
        d2 = d - dateutil.relativedelta.relativedelta(days=1)
        self.listing.start_date = d2

        d3 = d + dateutil.relativedelta.relativedelta(days=1)

        # try to change the date to start tomorrow
        self.listing.changeStartDate(d3)
        self.assertNotEqual(self.listing.start_date, d3)

    def test_changestartdate_beforestart(self):

        #modify date so it starts tomorrow
        d = datetime.now()
        d2 = d + dateutil.relativedelta.relativedelta(days=1)
        self.listing.start_date = d2

        d3 = d + dateutil.relativedelta.relativedelta(days=7)

        #try to change the date to start next week
        self.listing.changeStartDate(d3)
        self.assertEqual(self.listing.start_date, d3)

    def test_setEndDate_beforestart(self):
        sd = self.listing.start_date
        d3 =sd - dateutil.relativedelta.relativedelta(days=1)
        self.listing.setEndDate(d3)
        self.assertNotEqual(self.listing.end_date, d3)

    def test_setEndDate_afterstart(self):
        sd = self.listing.start_date
        d3 =sd + dateutil.relativedelta.relativedelta(days=1)
        self.listing.setEndDate(d3)
        self.assertEqual(self.listing.end_date, d3)

    def test_setCost_positive(self):
        self.listing.setCost(100)
        self.assertEqual(self.listing.getCost(), 100)

    def test_setCost_negative(self):
        self.assertEqual(self.listing.setCost(-1), None)

    def test_set_listingID(self):
        self.listing.setListingID(2)
        self.assertEqual(self.listing.listing_id, 2)

    def test_get_listingID(self):
        self.assertEqual(self.listing.getListingID(), 0)
    '''
    def test_add_tags(self):
        self.listing.addTags('abc')
        self.assertTrue(any("abc" in s for s in self.listing.tags))

    def test_add_tags_existing(self):
        self.listing.addTags('abc')
        self.listing.addTags('abc')
        tmp = Counter(self.listing.tags)
        self.assertEqual(tmp['abc'], 1)

    def test_remove_tags_exists(self):
        self.listing.addTags('abc')
        self.listing.removeTags('abc')
        self.assertFalse(any("abc" in s for s in self.listing.getTags()))

    def test_remove_tags_notthere(self):
        self.assertEqual(self.listing.removeTags('tag not existent'), None)
    '''
    def test_getStartDate(self):
        self.assertEqual(self.listing.getStartDate(), self.listing.start_date)

    def test_getEndDate(self):
        self.assertEqual(self.listing.getEndDate(), self.listing.end_date)

    def test_getType(self):
        self.assertEqual(self.listing.getType(), 'normal')
    # self.type = 'normal'  # default other options: category-featured, home-page-featured

    def test_type_category_featured(self):
        self.listing.updateType('category-featured')
        self.assertEqual(self.listing.getType(), 'category-featured')

    def test_type_home_page(self):
        self.listing.updateType('home-page-featured')
        self.assertEqual(self.listing.getType(), 'home-page-featured')

    def test_type_to_random(self):
        self.listing.updateType('random')
        self.assertEqual(self.listing.getType(), 'normal')

    def test_to_dict(self):
        list_dict = {'cust_id': 0, 'start_date': 'Jun 10 2015 01:33PM',
                     'end_date': 'Jun 17 2015 01:33PM', 'content': 'Default Content',
                     'listing_id': 0, 'cost': 100, 'type': 'normal',
                     'image': 'path/to/image', 'path': [], 'profit': 14.285714285714286,
                     'website': 'somewebsite.com', 'active': False}
        self.listing.set_init_date(datetime.strptime('Jun 10 2015  1:33PM', '%b %d %Y %I:%M%p'))
        self.listing.setEndDate(datetime.strptime('Jun 17 2015  1:33PM', '%b %d %Y %I:%M%p'))
        self.assertDictEqual(self.listing.to_dict(), list_dict)

    def test_to_string(self):
        strtmp = '0 Jun 10 2015 01:33PM Jun 17 2015 01:33PM Default Content 0 100 normal'
        self.listing.set_init_date(datetime.strptime('Jun 10 2015  1:33PM', '%b %d %Y %I:%M%p'))
        self.listing.setEndDate(datetime.strptime('Jun 17 2015  1:33PM', '%b %d %Y %I:%M%p'))
        self.assertEqual(self.listing.toString(), strtmp)

    def test_search_found(self):
        self.assertTrue(self.listing.search('Default'))

    def test_search_notfound(self):
        self.assertFalse(self.listing.search('NotFound'))

    def test_getCustID(self):
        self.assertEqual(self.listing.getCustID(), 0)

    def test_isActive_nonactive(self):
        self.listing.set_init_date(datetime.strptime('Jun 10 2015  1:33PM', '%b %d %Y %I:%M%p'))
        self.listing.setEndDate(datetime.strptime('Jun 17 2015  1:33PM', '%b %d %Y %I:%M%p'))
        self.assertFalse(self.listing.isActive())

    def test_isActive_active(self):
        today = datetime.now()
        tommorow = today + dateutil.relativedelta.relativedelta(
            days=1)
        self.listing.setEndDate(tommorow)
        self.assertTrue(self.listing.isActive())

    def test_set_and_get_imagepath(self):
        self.listing.setImagePath('/this/path')
        self.assertEqual(self.listing.getImagePath(), '/this/path')

    def test_set_and_get_website(self):
        self.listing.setWebsite('www.mywebsite.com')
        self.assertEqual(self.listing.getWebsite(), 'www.mywebsite.com')

    def test_set_and_get_path(self):
        self.listing.setPath(['soccer', 'gear'])
        self.assertEqual(self.listing.getPath(), ['soccer', 'gear'])

if __name__ == '__main__':
    unittest.main()
