import dateutil

from src.Interactors.Reporting import Reporting
import unittest2 as unittest
from src.Customer.AllCustomers import AllCustomers
from src.Categories.HomeCategory import HomeCategory
from datetime import datetime
from src.Listing.Listing import Listing
import dateutil.relativedelta


class testReporting(unittest.TestCase):
    def setUp(self):
        self.s = Reporting()
        self.maxDiff = None

    def create_fake_customers(self):
        d_now = datetime.now()
        d_lastweek = d_now - dateutil.relativedelta.relativedelta(days=7)
        d_lastmonth = d_now - dateutil.relativedelta.relativedelta(months=1)
        d_last8months = d_now - dateutil.relativedelta.relativedelta(months=8)
        d_last5months = d_now - dateutil.relativedelta.relativedelta(months=5)

        st = [(1, u'New name', u'New email2', d_last5months.strftime('%b %d %Y %I:%M%p')),
              (2, u'test name2', u'test2@gmail.com', d_lastmonth.strftime('%b %d %Y %I:%M%p')),
              (3, u'test name3', u'test3@gmail.com', d_last8months.strftime('%b %d %Y %I:%M%p'))]
        allcust = AllCustomers(st)
        return allcust.getAllCustomers()

    def create_fake_3lvl_cat_withlistings(self):
        home_cat = HomeCategory()
        d_now = datetime.now()
        d_yesterday = d_now - dateutil.relativedelta.relativedelta(days=1)
        d_lastweek = d_now - dateutil.relativedelta.relativedelta(days=7)
        d_lastmonth = d_now - dateutil.relativedelta.relativedelta(months=1)
        d_last2months = d_now - dateutil.relativedelta.relativedelta(months=2)
        d_last5months = d_now - dateutil.relativedelta.relativedelta(months=5)
        d_tommorow = d_now + dateutil.relativedelta.relativedelta(days=1)
        d_nextweek = d_now + dateutil.relativedelta.relativedelta(days=7)
        d_nextmonth = d_now + dateutil.relativedelta.relativedelta(months=1)

        l_home = Listing(0)
        l_home.updateType('home-page-featured')
        l_home.updateContent('This is a home page listing')
        l_home.setCost(2500)
        l_home.set_init_date(d_yesterday)
        l_home.setEndDate(d_nextmonth)
        home_cat.addListing(l_home)

        l = Listing(0)
        l.set_init_date(d_lastmonth)
        l.setEndDate(d_nextweek)
        l.updateContent('where is waldo in sports: normal')
        l.setCost(300)

        l_1 = Listing(0)
        l_1.set_init_date(d_last5months)
        l_1.setEndDate(d_last2months)
        l_1.updateContent('where is waldo in sports: category featured')
        l_1.updateType('category-featured')
        l_1.setCost(400)

        cat_sports = home_cat.addSubCategory('sports')
        cat_sports['sports'].addListing(l)
        cat_sports['sports'].addListing(l_1)

        l2 = Listing(0)
        l2.set_init_date(d_last2months)
        l2.setEndDate(d_nextmonth)
        l2.setCost(250)
        l2.updateContent('waldo is in sports featured')

        cat_soccer = cat_sports['sports'].addSubCategory('soccer')
        cat_soccer['soccer'].addListing(l2)

        l3 = Listing(0)
        l3.set_init_date(d_lastweek)
        l3.setEndDate(d_nextmonth)
        l3.updateType('category-featured')
        l3.updateContent('waldo is in sports featured')

        cat_gear = cat_soccer['soccer'].addSubCategory('gear')
        cat_gear['gear'].addListing(l3)

        return home_cat

    def test_getCustomers_6months_two(self):
        res = self.s.NewCustomersMonths(6, self.create_fake_customers())
        print(res)
        self.assertEqual(len(res), 2)

    def test_getCustomers_12months_three(self):
        res = self.s.NewCustomersMonths(10, self.create_fake_customers())
        print(res)
        self.assertEqual(len(res), 3)

    def test_getHierchy_dict(self):
        home_cat = self.create_fake_3lvl_cat_withlistings()
        total_count, home_dict_fun = self.s.getActiveListingHierchy(home_cat)

        gear_dict = {'gear': {'normal': 0, 'category-featured': 1, 'sub': []}}
        soccer_dict = {'soccer': {'normal': 1, 'category-featured': 0, 'sub': [gear_dict]}}
        sports_dict = {'sports': {'normal': 1, 'category-featured': 0, 'sub': [soccer_dict]}}
        home_dict = {'home-featured': 1, 'sub': [sports_dict]}

        expected_res = (4, home_dict)

        self.assertDictEqual(home_dict_fun, expected_res[1])

    def test_getHierchy_count(self):
        home_cat = self.create_fake_3lvl_cat_withlistings()
        total_count, home_dict_fun = self.s.getActiveListingHierchy(home_cat)

        gear_dict = {'gear': {'normal': 0, 'category-featured': 1, 'sub': []}}
        soccer_dict = {'soccer': {'normal': 1, 'category-featured': 0, 'sub': [gear_dict]}}
        sports_dict = {'sports': {'normal': 1, 'category-featured': 0, 'sub': [soccer_dict]}}
        home_dict = {'home-featured': 1, 'sub': [sports_dict]}

        expected_res = (4, home_dict)

        self.assertEqual(total_count, expected_res[0])

if __name__ == '__main__':
    unittest.main()
