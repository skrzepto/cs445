from src.Categories.SubCategory import SubCategory

class HomeCategory(dict):

    def __init__(self,*arg,**kw):
        super(HomeCategory, self).__init__(*arg, **kw)
        self['home-featured'] = []
        self['sub-categories'] = []

    def getSubCategoryByName(self, name):
        index = self.isSubCategoryValid(str(name).lower())
        if index > -1:
            return self['sub-categories'][index][str(name)]
        else:
            return None

    def isSubCategoryValid(self, cat):
        for index, cat_dict in enumerate(self['sub-categories']):
            for key in cat_dict:
                if key == str(cat).lower():
                    return index
        return -1

    def addListing(self, list):
        if list.getType() == 'home-page-featured':
            self['home-featured'].append(list)
            return self['home-featured'][-1]
        else:
            return None

    def getActiveListing(self, nam):
        result = []
        for l in self[str(nam)]:
            if l.isActive():
                result.append(l)
        return result

    def getActiveHomePageFeatured(self):
        return self.getActiveListing('home-featured')

    def depth_first_search_subcategories(self, sch):
        result = []

        for cat_dict in self['sub-categories']:
            for key in cat_dict:
                cat = cat_dict[key]
                result.extend(cat.depth_first_search(str(sch)))

        return result

    def get_cat_featured(self, path):
        if len(path) > 0:
            c = self.getSubCategoryByName(str(path[0]))
            if c:
                tmp = c.get_cat_featured(path[1:])
                if tmp is not None:
                    return tmp
                else:
                    return None
            else:
                return None
        else:
            return None

    def traverse(self, path):

        if len(path) > 0:
            c = self.getSubCategoryByName(str(path[0]))
            if c:
                if len(path[1:]) >0:
                    return c.traverse(path[1:])
                else:
                    return c
            else:
                return None
        elif len(path) ==0:
            return self


    def addSubCategory(self, name):
        s = SubCategory()
        s.setName(name)
        new_cat = {str(name): s}
        self['sub-categories'].append(new_cat)
        return self['sub-categories'][-1]

    def getAllSubCategories(self):
        return self['sub-categories']

    def getListingByCustomer(self, c_id):
        res = []

        for list in self['home-featured']:
            if list.getCustID() == int(c_id):
                res.append(list)

        for cat_dict in self['sub-categories']:
            for key in cat_dict:
                cat = cat_dict[key]
                res.extend(cat.getListingCustomerID(c_id))

        return res

    def getCategoryNames(self):
        res = []

        for d in self.getAllSubCategories():
            for keys in d:
                res.append(keys)

        return res






