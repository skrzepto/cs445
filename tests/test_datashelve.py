
import unittest
import subprocess
from src.Customer.AllCustomers import AllCustomers
from src.sad import SAD
from src.Listing.Listing import Listing
from datetime import datetime
import dateutil.relativedelta
import os,re

from src.Persistence.datashelve_t import datashelve_t

class testDataShelve(unittest.TestCase):
    ClassIsSetup = False

    def setUp(self):
        if not self.ClassIsSetup:
            self.tearDown()
            self.ClassIsSetup = True

        self.sad = SAD()
        self.create3lvl()

    def tearDown(self):
        try:
            os.remove('test_data')
        except:
            pass
        try:
            os.remove('test_data.db')
        except:
            pass

        try:
            os.remove('production_data')
        except:
            pass

        try:
            os.remove('production_data.db')
        except:
            pass

    def test_keyeror_capture(self):
        db = datashelve_t('production')
        self.assertIsNone(db.getData(), 'KeyError')

    def test_setandgetData_production(self):
        db = datashelve_t('production')
        db.storeData(self.sad)
        self.assertIsInstance(db.getData(), SAD, 'production data is loaded and stored and retrieval is a SAD class')

    def test_setandgetData_test(self):
        db = datashelve_t('test')
        db.storeData(self.sad)
        self.assertIsInstance(db.getData(), SAD, 'test data is loaded and stored and retrieval is a SAD class')



    def create3lvl(self):
        d_now = datetime.now()
        d_yesterday = d_now - dateutil.relativedelta.relativedelta(days=1)
        d_lastweek = d_now - dateutil.relativedelta.relativedelta(days=7)
        d_lastmonth = d_now - dateutil.relativedelta.relativedelta(months=1)
        d_last2months = d_now - dateutil.relativedelta.relativedelta(months=2)
        d_last5months = d_now - dateutil.relativedelta.relativedelta(months=5)

        d_tommorow = d_now + dateutil.relativedelta.relativedelta(days=1)
        d_nextweek = d_now + dateutil.relativedelta.relativedelta(days=7)
        d_nextmonth = d_now + dateutil.relativedelta.relativedelta(months=1)

        l_home = Listing(0, 1)
        l_home.updateType('home-page-featured')
        l_home.updateContent('This is a home page listing')
        l_home.setCost(2500)
        l_home.set_init_date(d_yesterday)
        l_home.setEndDate(d_nextmonth)
        self.sad.home_cat.addListing(l_home)

        l = Listing(1)
        l.set_init_date(d_lastmonth)
        l.setEndDate(d_nextweek)
        l.updateContent('where is waldo in sports: normal')
        l.setCost(300)

        l_1 = Listing(2)
        l_1.set_init_date(d_last5months)
        l_1.setEndDate(d_nextmonth)
        l_1.updateContent('where is waldo in sports: category featured')
        l_1.updateType('category-featured')
        l_1.setCost(400)

        cat_sports = self.sad.home_cat.addSubCategory('sports')
        cat_sports['sports'].addListing(l)
        cat_sports['sports'].addListing(l_1)

        l2 = Listing(3)
        l2.set_init_date(d_last2months)
        l2.setEndDate(d_nextmonth)
        l2.setCost(250)
        l2.updateContent('waldo is in sports featured')

        cat_soccer = cat_sports['sports'].addSubCategory('soccer')
        cat_soccer['soccer'].addListing(l2)

        l3 = Listing(1)
        l3.set_init_date(d_lastweek)
        l3.setEndDate(d_nextmonth)
        l3.updateType('category-featured')
        l3.updateContent('waldo is in sports featured')

        cat_gear = cat_soccer['soccer'].addSubCategory('gear')
        cat_gear['gear'].addListing(l3)

        print(self.sad.home_cat)

        self.sad.customers = self.createfakeCust()

    def createfakeCust(self):
        d_now = datetime.now()
        d_lastweek = d_now - dateutil.relativedelta.relativedelta(days=7)
        d_lastmonth = d_now - dateutil.relativedelta.relativedelta(months=1)
        d_last8months = d_now - dateutil.relativedelta.relativedelta(months=8)
        d_last5months = d_now - dateutil.relativedelta.relativedelta(months=5)

        st = [(1, u'New name', u'New email2', d_last5months.strftime('%b %d %Y %I:%M%p')),
              (2, u'test name2', u'test2@gmail.com', d_lastmonth.strftime('%b %d %Y %I:%M%p')),
              (3, u'test name3', u'test3@gmail.com', d_last8months.strftime('%b %d %Y %I:%M%p'))]
        allcust = AllCustomers(st)
        return allcust

if __name__ == '__main__':
    unittest.main()
