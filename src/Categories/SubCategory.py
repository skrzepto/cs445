
class SubCategory(dict):

    def __init__(self, *arg, **kw):
        super(SubCategory, self).__init__(*arg, **kw)
        self['name'] = str(arg)
        self['category-featured'] = []
        self['normal'] = []
        self['sub-categories'] = []

    def setName(self, name):
        self['name'] = str(name).lower()
        return str(name)

    def getName(self):
        return self['name']

    def addSubCategory(self, name):
        s = SubCategory()
        s.setName(name)
        new_cat = {str(name): s}
        self['sub-categories'].append(new_cat)
        return self['sub-categories'][-1]

    def getSubCategoryByName(self, name):
        res, index = self.isSubCategoryValid(str(name).lower())
        if res:
            return self['sub-categories'][index][str(name)]
        else:
            return None

    def getAllSubCategories(self):
        return self['sub-categories']

    def addListing(self, list):
        if list.getType() == 'normal':
            nextid = self.getmaxid() + 1
            list.setListingID(nextid)
            self['normal'].append(list)
            return self['normal'][-1]

        elif list.getType() == 'category-featured':
            nextid = self.getmaxid() + 1
            list.setListingID(nextid)
            self['category-featured'].append(list)
            return self['category-featured'][-1]

        else:
            return None

    def getActiveListing(self, nam):
        result = []
        for l in self[str(nam)]:
            if l.isActive():
                result.append(l)
        return result

    def getActiveNormalListing(self):
        return self.getActiveListing('normal')

    def getActiveFeaturedListing(self):
        return self.getActiveListing('category-featured')

    def getAllNormalListing(self):
        return self['normal']

    def getAllFeaturedListing(self):
        return self['category-featured']

    def search_all(self, sch):

        result = []

        for l in self['category-featured']:
            if l.search(str(sch)):
                result.append(l)

        for l in self['normal']:
            if l.search(str(sch)):
                result.append(l)

        return result



    def depth_first_search(self, sch):
        result = []

        result.extend(self.search_all(str(sch)))

        for cat_dict in self['sub-categories']:
            for key in cat_dict:
                cat = cat_dict[key]
                result.extend(cat.depth_first_search(str(sch)))

        return result

    def isSubCategoryValid(self, cat):
        for index, cat_dict in enumerate(self['sub-categories']):
            for key in cat_dict:
                if key == str(cat).lower():
                    return True, index
        return False, -1

    def get_cat_featured(self, path):
        if len(path) > 0:
            c = self.getSubCategoryByName(str(path[0]))
            if c:
                result = []
                result.extend(self.getActiveFeaturedListing())
                tmp = c.get_cat_featured(path[1:])
                if tmp is not None:
                    result.extend(tmp)
                    return result
                else:
                    return None  # this was result
            else:
                return None
        else:
            return self.getActiveFeaturedListing()

    def traverse(self, path):
        if len(path) > 0:
            c = self.getSubCategoryByName(str(path[0]))
            if c:
                if (len(path)) == 1:
                    return c
                else:
                    tmp = c.traverse(path[1:])
                    if tmp:
                        return tmp
                    else:
                        return None
            else:
                return None
        else:
            return None

    def getmaxid(self):
        maxid = 0
        for list in self['normal']:
            if list.getListingID() > maxid:
                maxid = list.getListingID()

        for list in self['category-featured']:
            if list.getListingID() > maxid:
                maxid = list.getListingID()

        return maxid

    def getListingByID(self, l_id):
        for list in self['normal']:
            if list.getListingID() == int(l_id):
                return list

        for list in self['category-featured']:
            if list.getListingID() == int(l_id):
                return list

        return None

    def getCategoryNames(self):
        res = []

        for d in self.getAllSubCategories():
            for keys in d:
                res.append(keys)

        return res

    def isCategoryEmpty(self):
        if self['category-featured'] != [] or self['normal'] != [] or self['sub-categories'] != []:
            return False
        else:
            return True

    def getSubCatIndex(self, name):
        res, index = self.isSubCategoryValid(str(name).lower())
        if res:
            return index
        else:
            return None

    def removeSubCategory(self, sub_cat_name):
        sub_cat = self.getSubCategoryByName(str(sub_cat_name))
        if sub_cat:
            if sub_cat.isCategoryEmpty():
                self['sub-categories'].pop(self.getSubCatIndex(str(sub_cat_name)))
                return True

        return None

    def getListingCustomerID(self, c_id):
        res = []

        for list in self['normal']:
            if list.getCustID() == int(c_id):
                res.append(list)

        for list in self['category-featured']:
            if list.getCustID() == int(c_id):
                res.append(list)

        for cat_dict in self['sub-categories']:
            for key in cat_dict:
                cat = cat_dict[key]
                res.extend(cat.getListingCustomerID(c_id))

        return res
