from src.Listing.Listing import Listing
from src.Customer.Customer import Customer
import datetime


class JsonToObjects:

    def createCustomer(self, json_data):

        if all (k in json_data for k in ("name", "email")):
            new_cust = Customer(json_data['name'], json_data['email'])

            if 'business_name' in json_data:
                new_cust.setBusinessName(json_data['business_name'])

            if 'registration_date' in json_data:
                reg_date = datetime.datetime.now()
                try:
                    # example str input 'Jun 1 2005  1:33PM'
                    reg_date = datetime.datetime.strptime(json_data['registration_date'], '%b %d %Y %I:%M%p')

                except:
                    print("Registration date passed in was wrong. Defaulting to now")
                    reg_date = datetime.datetime.now()

                finally:
                    new_cust.setRegisterDate(reg_date)
            else:
                new_cust.setRegisterDate(datetime.datetime.now())

            if 'phone' in json_data:
                new_cust.setPhone(json_data['phone'])

            if 'fb' in json_data:
                new_cust.setFB(json_data['fb'])

            if 'twitter' in json_data:
                new_cust.setTwitter(json_data['twitter'])

            if 'linkedin' in json_data:
                new_cust.setLinkedin(json_data['linkedin'])

            return new_cust

        else:
            return None

    def editCustomer(self, json_data, cust):
        # I am assuming this function is being called after a customer is found
        if "customer_id" in json_data:

            if 'name' in json_data:
                cust.update_name(json_data['name'])

            if 'business_name' in json_data:
                cust.setBusinessName(json_data['business_name'])

            if 'registration_date' in json_data:
                try:
                    # example str input 'Jun 1 2005  1:33PM'
                    reg_date = datetime.datetime.strptime(json_data['registration_date'], '%b %d %Y %I:%M%p')
                    cust.setRegisterDate(reg_date)
                except:
                    print("Registration date passed in was wrong. Defaulting to now")

            if 'phone' in json_data:
                cust.setPhone(json_data['phone'])

            if 'fb' in json_data:
                cust.setFB(json_data['fb'])

            if 'twitter' in json_data:
                cust.setTwitter(json_data['twitter'])

            if 'linkedin' in json_data:
                cust.setLinkedin(json_data['linkedin'])

            if 'email' in json_data:
                cust.update_email(json_data['email'])

            return cust

        else:
            return None

    def addListing(self, json_data):
        # do i check if the path is correct before of after creating this listing?
        # also do I check if the customer exists before creating this listing?
        if all(k in json_data for k in ("customer_id", "start_date", "end_date", "content", "cost", "path")):
            try:
                cust_id = int(json_data['customer_id'])

            except:
                return None

            new_listing = Listing(cust_id)

            try:
                # example str input 'Jun 1 2005  1:33PM'
                new_listing.set_init_date(datetime.datetime.strptime(json_data['start_date'], '%b %d %Y %I:%M%p'))
                if not new_listing.setEndDate(datetime.datetime.strptime(json_data['end_date'], '%b %d %Y %I:%M%p')):
                    print("Listing end date is before start date")
                    return None

            except:
                print("Date is passed in wrong. Example 'Jun 1 2005  1:33PM' ")
                return None


            new_listing.updateContent(json_data['content'])
            new_listing.setPath(json_data['path'])

            # I need to make sure cost is an int
            try:
                cost = int(json_data['cost'])
                if cost >=0:
                    new_listing.setCost(cost)
                else:
                    print("Cost is passed in wrong. make sure its positive and an number ")
                    return None
            except:
                print("Cost is passed in wrong. make sure its positive and an number ")
                return None

            if 'type' in json_data:
                new_listing.updateType(json_data['type']) # should we error check if type is wrong and report back or no?

            if 'website' in json_data:
                new_listing.setWebsite(json_data['website'])

            if 'image' in json_data:
                new_listing.setImagePath(json_data['image'])

            return new_listing

        else:
            return None