import unittest2 as unittest
from src.Actors.CSR import CSR
from src.Customer.AllCustomers import AllCustomers
from src.sad import SAD
from src.Listing.Listing import Listing
from datetime import datetime
import dateutil.relativedelta
from src.Customer.Customer import Customer

class testCSR(unittest.TestCase):

    def setUp(self):
        self.sad = SAD()
        self.create3lvl_with_fake_customers()
        self.csr = CSR(self.sad)
        self.adv = self.sad.customers

    def test_getAllCustomers(self):
        res = self.csr.getAllCustomers()
        self.assertEqual(len(res), 3)

    def test_search_advertisers_found(self):
        tmp = self.csr.search_customers('test2')
        reg_date = datetime.now() - dateutil.relativedelta.relativedelta(months=1)
        cust_dict ={'business': 'business name',
                    'cust_id': '2',
                    'email': 'test2@gmail.com',
                    'facebook': 'fb.com/user',
                    'linkedin': 'linkedin.com/user',
                    'name': 'test name2',
                    'phone': '8675309',
                    'registered': reg_date.strftime('%b %d %Y %I:%M%p'),
                    'twitter': 'twitter.com/user'}

        self.assertEqual(tmp, [cust_dict])

    def test_search_advertisers_empty(self):
        tmp = self.csr.search_customers('none')
        self.assertEqual(tmp, [])

    def test_selectadvertiserbyid_valid(self):
        tmp = self.csr.select_customer_id(2)

        reg_date = datetime.now() - dateutil.relativedelta.relativedelta(months=1)

        expected = {'email': 'test2@gmail.com', 'cust_id': '2', 'name': 'test name2',
                    'business': 'business name', 'facebook': 'fb.com/user',
                    'linkedin': 'linkedin.com/user', 'phone': '8675309',
                    'registered': reg_date.strftime('%b %d %Y %I:%M%p'), 'twitter': 'twitter.com/user'}
        self.assertDictEqual(tmp, expected)

    def test_selectadvertiserbyid_notfound(self):
        tmp = self.csr.select_customer_id(9999)
        self.assertIsNone(tmp)

    def test_getListingByCustomer_happy(self):
        res = self.csr.get_all_listing_for_customer(0)
        print(res)
        self.assertEqual(len(res), 5)

    def test_getListingByCustomer_nolistings(self):
        res = self.csr.get_all_listing_for_customer(99999)
        print(res)
        self.assertEqual(res, [])

    def test_search(self):
        res = self.csr.search("waldo")
        print(res)

        self.assertEqual(len(res['listings']), 4)

    def create3lvl_with_fake_customers(self):
        d_now = datetime.now()
        d_lastweek = d_now - dateutil.relativedelta.relativedelta(days=7)
        d_lastmonth = d_now - dateutil.relativedelta.relativedelta(months=1)
        d_last8months = d_now - dateutil.relativedelta.relativedelta(months=8)
        d_last5months = d_now - dateutil.relativedelta.relativedelta(months=5)

        st = [(1, u'New name', u'New email2', d_last5months.strftime('%b %d %Y %I:%M%p')),
              (2, u'test name2', u'test2@gmail.com', d_lastmonth.strftime('%b %d %Y %I:%M%p')),
              (3, u'test name3', u'test3@gmail.com', d_last8months.strftime('%b %d %Y %I:%M%p'))]
        allcust = AllCustomers(st)

        self.sad.customers = allcust

        d_yesterday = d_now - dateutil.relativedelta.relativedelta(days=1)
        d_last2months = d_now - dateutil.relativedelta.relativedelta(months=2)
        d_last5months = d_now - dateutil.relativedelta.relativedelta(months=5)

        d_tommorow = d_now + dateutil.relativedelta.relativedelta(days=1)
        d_nextweek = d_now + dateutil.relativedelta.relativedelta(days=7)
        d_nextmonth = d_now + dateutil.relativedelta.relativedelta(months=1)

        l_home = Listing(0, 1)
        l_home.updateType('home-page-featured')
        l_home.updateContent('This is a home page listing')
        l_home.setCost(2500)
        l_home.set_init_date(d_yesterday)
        l_home.setEndDate(d_nextmonth)
        self.sad.home_cat.addListing(l_home)

        l = Listing(0)
        l.set_init_date(d_lastmonth)
        l.setEndDate(d_nextweek)
        l.updateContent('where is waldo in sports: normal')
        l.setCost(300)

        l_1 = Listing(0)
        l_1.set_init_date(d_last5months)
        l_1.setEndDate(d_last2months)
        l_1.updateContent('where is waldo in sports: category featured')
        l_1.updateType('category-featured')
        l_1.setCost(400)

        cat_sports = self.sad.home_cat.addSubCategory('sports')
        cat_sports['sports'].addListing(l)
        cat_sports['sports'].addListing(l_1)

        l2 = Listing(0)
        l2.set_init_date(d_last2months)
        l2.setEndDate(d_nextmonth)
        l2.setCost(250)
        l2.updateContent('waldo is in sports featured')

        cat_soccer = cat_sports['sports'].addSubCategory('soccer')
        cat_soccer['soccer'].addListing(l2)

        l3 = Listing(0)
        l3.set_init_date(d_lastweek)
        l3.setEndDate(d_nextmonth)
        l3.updateType('category-featured')
        l3.updateContent('waldo is in sports featured')

        cat_gear = cat_soccer['soccer'].addSubCategory('gear')
        cat_gear['gear'].addListing(l3)
        emp = cat_gear['gear'].addSubCategory('empty')

        print(self.sad.home_cat)