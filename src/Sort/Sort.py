import random
from operator import itemgetter
import datetime

class SortListings:

    def profitability(self, arr):
        newlist = sorted(arr, key=itemgetter('profit'), reverse=True)
        return newlist

    def random(self, arr):
        shuffled = sorted(arr, key=lambda k: random.random())
        return shuffled

    def name(self, arr, cust_dict):

        # https://stackoverflow.com/questions/15650348/sorting-a-list-of-dictionaries-based-on-the-order-of-values-of-another-list
        '''
        The simplest way would be to use list.index to generate a sort value for your list of dictionaries:

        listTwo.sort(key=lambda x: listOne.index(x["eyecolor"]))

        This is a little bit inefficient though, since list.index does a linear search through the eye-color list.
        If you had many eye colors to check against, it would be slow.
        A somewhat better approach would build an index dictionary instead:

        order_dict = {color: index for index, color in enumerate(listOne)}
        listTwo.sort(key=lambda x: order_dict[x["eyecolor"]])

        If you don't want to modify listTwo, you can use the built-in sorted function instead of the list.sort method.
        It returns a sorted copy of the list, rather than sorting in-place

        '''
        #I can add 'name' to the dict sort it and then remove it right before I return it

        for listing in arr:
            for customer in cust_dict:
                if listing['cust_id'] == customer['cust_id']:
                    listing['name'] = customer['name']

        newlist = sorted(arr, key=itemgetter('name'), reverse=False)

        for listing in newlist:
            del listing['name']

        return newlist

    def start_date(self, arr):

        for listing in arr:
            listing['start_date'] = datetime.datetime.strptime(listing['start_date'], '%b %d %Y %I:%M%p')

        newlist = sorted(arr, key=itemgetter('start_date'), reverse=False)

        for listing in newlist:
            listing['start_date'] = listing['start_date'].strftime('%b %d %Y %I:%M%p')

        return newlist


