
class CSR:
    def __init__(self, sad):
        self.sad = sad

    def search_customers(self, srch):
        res = self.sad.customers.search_all_customers(str(srch))
        res = [cust.to_dict() for cust in res]
        return res


    def search(self, srch):
        res = self.sad.search(str(srch))
        return res


    def select_customer_id(self, id):
        res = self.sad.getAdvertiserbyID(int(id))
        if res:
            return res.to_dict()
        else:
            return None

    def get_all_listing_for_customer(self, id):
        res = self.sad.getListingByCustomer(int(id))
        if res is not None and res != []:
            res = [l.to_dict() for l in res]
            return res
        else:
            return []

    def getAllCustomers(self):
        res = self.sad.getAllAdvertiser()
        print(res)
        return res