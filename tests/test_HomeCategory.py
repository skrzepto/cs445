from src.Categories.HomeCategory import HomeCategory
from src.Listing.Listing import Listing
import unittest2 as unittest

import dateutil.relativedelta
from datetime import datetime


class testHomeCategory(unittest.TestCase):

    def setUp(self):
        self.home = HomeCategory()

    def testGetSubCategories_empty(self):
        self.assertEqual(self.home.getAllSubCategories(), [])

    def create3lvlsubcat(self):
        l = Listing(0, 1)
        l.updateContent('where is waldo in sports: normal')
        cat_sports = self.home.addSubCategory('sports')
        cat_sports['sports'].addListing(l)

        l2 = Listing(0, 2)
        l2.updateType('category-featured')
        l2.updateContent('waldo is in sports featured')

        cat_soccer = cat_sports['sports'].addSubCategory('soccer')
        cat_soccer['soccer'].addListing(l2)

        l3 = Listing(0, 3)
        l3.updateType('category-featured')
        l3.updateContent('waldo is in sports featured')

        cat_gear = cat_soccer['soccer'].addSubCategory('gear')
        cat_gear['gear'].addListing(l3)

    def test_subcatvalid(self):
        self.create3lvlsubcat()
        exists = self.home.getSubCategoryByName('sports')
        print(exists)
        notthere = self.home.getSubCategoryByName('notthere')

    def test_addhomempagelisting_valid(self):
        l = Listing()
        l.updateType('home-page-featured')
        self.assertEqual(self.home.addListing(l),l)

    def test_addHomePageListing_notvalid(self):
        l = Listing()
        self.assertIsNone(self.home.addListing(l))

    def test_getActivehomefeatured_none(self):
        tmp = self.home.getActiveHomePageFeatured()
        self.assertEqual(tmp, [])

    def test_getActivehomefeatured_one(self):
        l = Listing()
        l.updateType('home-page-featured')
        today = datetime.now()
        tommorow = today + dateutil.relativedelta.relativedelta(
            days=1)
        l.setEndDate(tommorow)
        self.home.addListing(l)
        self.assertEqual(self.home.getActiveHomePageFeatured(), [l])

    def test_dfs_find_waldo_exists(self):
        self.create3lvlsubcat()
        self.assertEqual(len(self.home.depth_first_search_subcategories('waldo')), 3)

    def test_dfs_find_waldo_nothing(self):
        self.assertEqual(len(self.home.depth_first_search_subcategories('waldo')), 0)

    def test_getCategoryFeatured_validpath(self):
        self.create3lvlsubcat()
        self.assertEqual(len(self.home.get_cat_featured(['sports', 'soccer', 'gear'])), 2)

    def test_getCategoryFeatured_invalidpath_1(self):
        self.assertIsNone(self.home.get_cat_featured(['sports', 'soccer', 'gear']))

    def test_getCatFeat_invalidpath_2(self):
        self.create3lvlsubcat()
        self.assertIsNone(self.home.get_cat_featured(['sports', 'soccer', 'cleats']))

    def test_getCatFeat_nopath(self):
        self.assertIsNone(self.home.get_cat_featured([]))

    def test_traverse_happy_path(self):
        self.create3lvlsubcat()
        self.assertIsNotNone(self.home.traverse(['sports', 'soccer']))

    def test_traverse_happy_path_2(self):
        self.create3lvlsubcat()
        self.assertIsNotNone(self.home.traverse(['sports']))

    def test_traverse_nopath_1(self):
        self.assertIsNone(self.home.traverse(['none']))

    def test_traverse_nopath_2(self):
        self.create3lvlsubcat()
        self.assertIsNone(self.home.traverse(['sports', 'none']))

    def test_traverse_emptypath(self):
        self.assertEqual(self.home.traverse([]), self.home)

    def test_getListingByCustomer_happy(self):
        self.create3lvlsubcat()
        res = self.home.getListingByCustomer(0)
        print(res)
        self.assertEqual(len(res), 3)

    def test_getListingByCustomer_nothing(self):
        self.create3lvlsubcat()
        res = self.home.getListingByCustomer(9999)
        print(res)
        self.assertEqual(res, [])

if __name__ == '__main__':
    unittest.main()
