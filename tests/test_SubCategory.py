from datetime import datetime
from src.Categories.SubCategory import SubCategory
from src.Listing.Listing import Listing
import unittest2 as unittest
import dateutil.relativedelta


class testSubCategory(unittest.TestCase):

    def setUp(self):
        self.sub = SubCategory()
        self.sub.setName('Sports')

    def create_3lvl(self):
        l = Listing(0, 1)
        l.updateContent('where is waldo in sports: normal')

        self.sub.addListing(l)

        l2 = Listing(0, 2)
        l2.updateType('category-featured')
        l2.updateContent('waldo is in sports featured')

        cat_soccer = self.sub.addSubCategory('soccer')
        cat_soccer['soccer'].addListing(l2)

        cat_gear = cat_soccer['soccer'].addSubCategory('gear')

    def testGetSubCategories_empty(self):
        self.assertEqual(self.sub.getAllSubCategories(), [])

    def testAddSubCategory(self):
        self.sub.addSubCategory('Soccer')
        print(self.sub['sub-categories'])
        self.assertIsNot(self.sub['sub-categories'], [])

    def test_addListing_normal_active(self):
        l = Listing(0, 1)
        today = datetime.now()
        tommorow = today + dateutil.relativedelta.relativedelta(
            days=1)
        l.setEndDate(tommorow)
        self.sub.addListing(l)
        self.assertEqual(self.sub.getActiveNormalListing()[0], l)

    def test_addListing_normal_nonactive(self):
        l = Listing(0, 1)
        today = datetime.now()
        yesterday = today - dateutil.relativedelta.relativedelta(
            days=1)

        twodaysago = today - dateutil.relativedelta.relativedelta(
            days=2)

        l.set_init_date(twodaysago)

        l.setEndDate(yesterday)
        self.sub.addListing(l)
        self.assertTrue(self.sub.getAllNormalListing()[0] == l and self.sub.getActiveNormalListing() == [])

    def test_addListing_featured_active(self):
        l = Listing(0, 1)
        l.updateType('category-featured')
        today = datetime.now()
        tommorow = today + dateutil.relativedelta.relativedelta(
            days=1)

        l.setEndDate(tommorow)
        self.sub.addListing(l)
        self.assertEqual(self.sub.getActiveFeaturedListing()[0], l)

    def test_addListing_featured_nonactive(self):
        l = Listing(0, 1)
        l.updateType('category-featured')
        today = datetime.now()
        yesterday = today - dateutil.relativedelta.relativedelta(
            days=1)

        twodaysago = today - dateutil.relativedelta.relativedelta(
            days=2)

        l.set_init_date(twodaysago)
        l.setEndDate(yesterday)
        self.sub.addListing(l)
        self.assertTrue(self.sub.getAllFeaturedListing()[0] == l and self.sub.getActiveFeaturedListing() == [])


    def test_addListing_notvalid(self):
        l = Listing(0, 1)
        l.updateType('home-page-featured')
        today = datetime.now()
        tommorow = today + dateutil.relativedelta.relativedelta(
            days=1)

        l.setEndDate(tommorow)
        self.assertIsNone(self.sub.addListing(l))

    def test_depth_first_search_onelvl(self):
        l = Listing(0, 1)
        l.updateContent('where is waldo in sports: normal')

        l2 = Listing(0, 2)
        l2.updateType('category-featured')
        l2.updateContent('waldo is in sports featured')

        self.sub.addListing(l)
        self.sub.addListing(l2)

        self.assertEqual(self.sub.depth_first_search('waldo'), [l2, l])


    def test_depth_first_search_twolvl(self):
        l = Listing(0, 1)
        l.updateContent('where is waldo in sports: normal')

        l2 = Listing(0, 2)
        l2.updateType('category-featured')
        l2.updateContent('waldo is in sports featured')

        self.sub.addListing(l)

        cat_soccer = self.sub.addSubCategory('soccer')
        cat_soccer['soccer'].addListing(l2)

        print(self.sub)

        self.assertEqual(self.sub.depth_first_search('waldo'), [l, l2])

    def test_isvalidcategory_no(self):
        self.sub.addSubCategory('soccer')
        t, i = self.sub.isSubCategoryValid('basketball')
        self.assertFalse(t)

    def test_isvalidcategory_yes(self):
        self.sub.addSubCategory('soccer')
        self.assertTrue(self.sub.isSubCategoryValid('soccer'))

    def test_getCategory_nothere(self):
        self.assertEqual(self.sub.getSubCategoryByName('soccer'), None)


    def test_getCategoryFeatured_2lvl_pathexists(self):
        l = Listing(0, 1)
        l.updateContent('where is waldo in sports: normal')

        self.sub.addListing(l)

        l2 = Listing(0, 2)
        l2.updateType('category-featured')
        l2.updateContent('waldo is in sports featured')

        cat_soccer = self.sub.addSubCategory('soccer')
        cat_soccer['soccer'].addListing(l2)

        tmp = self.sub.get_cat_featured(['soccer'])
        print(tmp)
        self.assertEqual(tmp, [l2])

    def test_getCategoryFeatured_3lvl_pathexists(self):
        self.create_3lvl()
        t = self.sub.get_cat_featured(['soccer'])
        g = self.sub.traverse(['soccer'])

        l2 = Listing(0, 2)
        l2.updateType('category-featured')
        l2.updateContent('waldo is in sports featured')
        g.addListing(l2)

        res = self.sub.get_cat_featured(['soccer', 'gear'])
        self.assertEqual(res, [t[0], l2])

    def test_getCategoryFeatured_3lvl_notexistent(self):
        self.create_3lvl()
        t = self.sub.get_cat_featured(['soccer'])
        g = self.sub.traverse(['soccer'])

        l2 = Listing(0, 2)
        l2.updateType('category-featured')
        l2.updateContent('waldo is in sports featured')
        g.addListing(l2)

        res = self.sub.get_cat_featured(['soccer', 'nothere'])
        self.assertIsNone(res)

    def test_traverse_happypath_2lvls(self):
        l = Listing(0, 1)
        l.updateContent('where is waldo in sports: normal')

        self.sub.addListing(l)

        l2 = Listing(0, 2)
        l2.updateType('category-featured')
        l2.updateContent('waldo is in sports featured')

        cat_soccer = self.sub.addSubCategory('soccer')
        cat_soccer['soccer'].addListing(l2)

        print(self.sub)
        tmp = self.sub.traverse(['soccer'])
        self.assertEqual(tmp, cat_soccer['soccer'])

    def test_traverse_nopath_2lvls(self):
        self.create_3lvl()

        print(self.sub)
        tmp = self.sub.traverse(['basketball'])
        self.assertEqual(tmp, None)

    def test_traverse_nopath_3lvls(self):
        self.create_3lvl()
        tmp = self.sub.traverse(['soccer', 'cleats'])
        self.assertEqual(tmp, None)

    def test_traverse_happypath_3lvls(self):
        l = Listing(0, 1)
        l.updateContent('where is waldo in sports: normal')

        self.sub.addListing(l)

        l2 = Listing(0, 2)
        l2.updateType('category-featured')
        l2.updateContent('waldo is in sports featured')

        cat_soccer = self.sub.addSubCategory('soccer')
        cat_soccer['soccer'].addListing(l2)

        cat_gear = cat_soccer['soccer'].addSubCategory('gear')

        tmp = self.sub.traverse(['soccer', 'gear'])
        self.assertEqual(tmp, cat_gear['gear'])

    def test_traverse_inputnopath(self):
        self.assertEqual(self.sub.traverse([]), None)

    def test_getmaxid_1(self):
        print(self.sub)
        self.assertEqual(self.sub.getmaxid(),0)

    def test_getmaxid_2(self):
        self.create_3lvl()
        self.assertEqual(self.sub.getmaxid(),1)

    def test_getlistingbyid(self):
        self.create_3lvl()
        tmp = self.sub.getListingByID(1)
        self.assertIsNotNone(tmp)

    def test_getlistingbyid_none(self):
        self.assertIsNone(self.sub.getListingByID(10))

    def test_getCategorynames_3lvl(self):
        self.create_3lvl()
        self.assertEqual(self.sub.getCategoryNames(), ['soccer'])

    def test_isCategoryEmpty_nothing(self):
        self.assertTrue(self.sub.isCategoryEmpty())

    def test_isCategoryEmpty_no(self):
        l = Listing(0, 1)
        today = datetime.now()
        yesterday = today - dateutil.relativedelta.relativedelta(
            days=1)
        twodaysago = today - dateutil.relativedelta.relativedelta(
            days=2)
        l.set_init_date(twodaysago)
        l.setEndDate(yesterday)
        self.sub.addListing(l)
        self.assertFalse(self.sub.isCategoryEmpty())

    def test_isCategoryEmpty_no_onelvl(self):
        sub = self.sub.addSubCategory('test')
        l = Listing(0, 1)
        today = datetime.now()
        yesterday = today - dateutil.relativedelta.relativedelta(
            days=1)
        twodaysago = today - dateutil.relativedelta.relativedelta(
            days=2)
        l.set_init_date(twodaysago)
        l.setEndDate(yesterday)
        sub['test'].addListing(l)
        self.assertFalse(self.sub.isCategoryEmpty())

    def test_getindexsubcat_valid(self):
        sub_cat = self.sub.addSubCategory('test')
        self.assertEqual(self.sub.getSubCatIndex('test'), 0)

    def test_getindexsubcat_valid(self):
        self.assertIsNone(self.sub.getSubCatIndex('test'))

    def test_removesubcat_valid(self):
        sub_cat = self.sub.addSubCategory('test')
        self.assertTrue(self.sub.removeSubCategory('test'))

    def test_removesubcat_notvalid(self):
        sub = self.sub.addSubCategory('test')
        l = Listing(0, 1)
        today = datetime.now()
        yesterday = today - dateutil.relativedelta.relativedelta(
            days=1)
        twodaysago = today - dateutil.relativedelta.relativedelta(
            days=2)
        l.set_init_date(twodaysago)
        l.setEndDate(yesterday)
        sub['test'].addListing(l)
        self.assertIsNone(self.sub.removeSubCategory('test'))

    def test_getListingCustomerID_happy(self):
        self.create_3lvl()
        res = self.sub.getListingCustomerID(0)
        print(res)
        self.assertEqual(len(res),2)

    def test_getListingCustomerID_nothing(self):
        self.create_3lvl()
        res = self.sub.getListingCustomerID(9999)
        print(res)
        self.assertEqual(len(res),0)

if __name__ == '__main__':
    unittest.main()
