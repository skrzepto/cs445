#! /bin/bash
cd ~

read -p "Instal dependecies? " -n 1 -r
echo    # (optional) move to a new line
if [[ $REPLY =~ ^[Yy]$ ]]
then

sudo apt-get -y install git
sudo apt-get -y install python3
sudo apt-get -y install python3-setuptools
sudo easy_install3 pip

git clone https://skrzepto@bitbucket.org/skrzepto/cs445.git

cd ./cs445

sudo pip install -r requirements.txt

echo 'export PYTHONPATH="${PYTHONPATH}:${HOME}/cs445:${HOME}/cs445/src:${HOME}/cs445/tests"' >> ~/.bashrc

source ~/.bashrc

fi


cd ~/cs445
sudo chmod +x ./setup.sh
py.test --cov=src tests/
./delete_data.sh


read -p "Did the tests pass? " -n 1 -r
echo    # (optional) move to a new line
if [[ $REPLY =~ ^[Yy]$ ]]
then


	cd ~/cs445/src/Delivery/REST
	read -p "Enter 1: production, 2: test " -n 1 -r
    echo    # (optional) move to a new line
    if [[ $REPLY =~ ^[1]$ ]]
	then
	    python3 rest_sad.py production
    elif [[ $REPLY =~ ^[2]$ ]]
    then
        python3 rest_sad.py test
    else
        echo "Invalid input"
	fi
	
 fi
exit 1




