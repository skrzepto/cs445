#!/usr/bin/env bash

find . -name "test_data" -delete
find . -name "test_data.db" -delete
find . -name "production_data" -delete
find . -name "production_data.db" -delete


