import unittest2 as unittest
from src.Persistence.create_data import create_data
from src.sad import SAD
class test_data_creation(unittest.TestCase):
    def setUp(self):
        self.data = create_data()

    def test_create(self):
        self.assertIsInstance(self.data.create3lvl_with_fake_customers(), SAD)