from src.Sort.Sort import SortListings
import unittest2 as unittest
from datetime import datetime
import dateutil.relativedelta


class testSort(unittest.TestCase):
    def setUp(self):
        d_now = datetime.now()

        d_yesterday = d_now - dateutil.relativedelta.relativedelta(days=1)
        d_lastweek = d_now - dateutil.relativedelta.relativedelta(days=7)
        d_lastmonth = d_now - dateutil.relativedelta.relativedelta(months=1)
        d_last2months = d_now - dateutil.relativedelta.relativedelta(months=2)
        d_last5months = d_now - dateutil.relativedelta.relativedelta(months=5)

        d_tommorow = d_now + dateutil.relativedelta.relativedelta(days=1)
        d_nextweek = d_now + dateutil.relativedelta.relativedelta(days=7)
        d_nextmonth = d_now + dateutil.relativedelta.relativedelta(months=1)

        self.listing_1 = {'cost': 100, 'type': 'category-featured', 'cust_id': 1,
                     'end_date': d_nextmonth.strftime('%b %d %Y %I:%M%p'),
                     'start_date': d_lastweek.strftime('%b %d %Y %I:%M%p'),
                     'content': 'waldo is in sports featured',
                     'listing_id': 1, 'image': 'path/to/image', 'path': [],
                     'profit': 100 / (d_nextmonth - d_lastweek).days, 'website': 'somewebsite.com', 'active': True}

        self.listing_2 = {'cost': 300, 'type': 'category-featured', 'cust_id': 2,
                     'end_date': d_nextweek.strftime('%b %d %Y %I:%M%p'),
                     'start_date': d_lastmonth.strftime('%b %d %Y %I:%M%p'),
                     'content': 'waldo is in sports featured',
                     'listing_id': 1, 'image': 'path/to/image', 'path': [],
                     'profit': 300 / (d_nextweek - d_lastmonth).days, 'website': 'somewebsite.com', 'active': True}

        self.listing_3 = {'cost': 1600, 'type': 'category-featured', 'cust_id': 3,
                     'end_date': d_nextmonth.strftime('%b %d %Y %I:%M%p'),
                     'start_date': d_last5months.strftime('%b %d %Y %I:%M%p'),
                     'content': 'waldo is in sports featured',
                     'listing_id': 1, 'image': 'path/to/image', 'path': [],
                     'profit': 1600 / (d_nextmonth - d_last5months).days, 'website': 'somewebsite.com', 'active': True}

        self.listing_4 = {'cost': 600, 'type': 'category-featured', 'cust_id': 4,
                     'end_date': d_nextmonth.strftime('%b %d %Y %I:%M%p'),
                     'start_date': d_last2months.strftime('%b %d %Y %I:%M%p'),
                     'content': 'waldo is in sports featured',
                     'listing_id': 1, 'image': 'path/to/image', 'path': [],
                     'profit': 600 / (d_nextmonth - d_last2months).days, 'website': 'listing4.com', 'active': True}

        self.data = [self.listing_2, self.listing_3, self.listing_4, self.listing_1]
        self.sort = SortListings()

    def testSort_random(self):
        print()
        print('Sort: Random')

        r1 = self.sort.random(self.data)
        r2 = self.sort.random(r1)

        print(r1)
        print(r2)

        while r1 == r2:
            r2 = self.sort.random(r1)

        self.assertNotEqual(r1, r2, 'listings should be randomly arranged')

    def testSort_date(self):
        print()
        print('Sort: Date')
        arranged_by_date = self.sort.start_date(self.data)
        expected = [self.listing_3, self.listing_4, self.listing_2, self.listing_1]

        print(arranged_by_date)
        self.assertEqual(arranged_by_date, expected, 'date is arranged by startdate')

    def testSort_profit(self):
        print()
        print('Sort: Profit')
        expected = [self.listing_3, self.listing_2, self.listing_4, self.listing_1]
        sorted_arr = self.sort.profitability(self.data)
        print(sorted_arr)
        self.assertEqual(sorted_arr, expected, 'Sorted by profitability')

    def testSort_name(self):
        # this might be a problem since I can't just sort on customer ID
        # I might need to make an array of dict of customers like {'customer_id': 3, 'name': 'Alice'}

        print()
        print('Sort: Name')

        cust_dict = [{'cust_id': 1, 'name': 'Zack Dorn'}, {'cust_id': 2, 'name': 'Tarun G'},
                     {'cust_id': 3, 'name': 'Alice M'}, {'cust_id': 4, 'name': 'Robert T'}]

        expected = [self.listing_3, self.listing_4, self.listing_2, self.listing_1]
        sorted_arr = self.sort.name(self.data, cust_dict)
        print(sorted_arr)

        self.assertEqual(expected, sorted_arr, 'sorting by name')

