Repo: https://bitbucket.org/skrzepto/cs445

Most difficult part of the assignment: was probably how to start the project and make sure it has a good foundation. Also, testing my rest routes was hard to setup and decided to just test the GET methods and hand test the POST, PUTS

Status for the assignment -- complete
Hours Spent: ~200-250


Main APP lines of code:

~~~
➜  cs445 git:(master) ✗ cloc ./src/ --exclude-dir=lib,frameworks
      26 text files.
      18 unique files.                              
       4 files ignored.

http://cloc.sourceforge.net v 1.60  T=0.16 s (113.5 files/s, 10836.3 lines/s)
-------------------------------------------------------------------------------
Language                     files          blank        comment           code
-------------------------------------------------------------------------------
Python                          18            416             32           1271
-------------------------------------------------------------------------------
SUM:                            18            416             32           1271
-------------------------------------------------------------------------------

~~~
   
Lines of unittest code

~~~
➜  cs445 git:(master) ✗ cloc ./tests/ --exclude-dir=lib,frameworks
      27 text files.
      27 unique files.                              
      10 files ignored.

http://cloc.sourceforge.net v 1.60  T=0.10 s (172.2 files/s, 22227.6 lines/s)
-------------------------------------------------------------------------------
Language                     files          blank        comment           code
-------------------------------------------------------------------------------
Python                          18            573             34           1717
-------------------------------------------------------------------------------
SUM:                            18            573             34           1717
-------------------------------------------------------------------------------

~~~
    
    
Cyclomatic complexity 

~~~
➜  cs445 git:(master) ✗ radon cc ./src --total-average
src/sad.py
    M 50:4 SAD.getListingbyPath - A
    M 102:4 SAD.sort - A
    M 12:4 SAD.search - A
    M 40:4 SAD.addListingbyPath - A
    M 85:4 SAD.traverse - A
    C 5:0 SAD - A
    M 23:4 SAD.getListingbyPathID - A
    M 78:4 SAD.getListingByCustomer - A
    M 94:4 SAD.editSortSetting - A
    M 7:4 SAD.__init__ - A
    M 31:4 SAD.getAdvertiserbyID - A
    M 36:4 SAD.getAllAdvertiser - A
src/Search/Search.py
    C 1:0 Search - A
    M 3:4 Search._search - A
src/Persistence/datashelve_t.py
    C 4:0 datashelve_t - A
    M 5:4 datashelve_t.__init__ - A
    M 9:4 datashelve_t.getData - A
    M 24:4 datashelve_t.storeData - A
src/Persistence/create_data.py
    C 7:0 create_data - A
    M 8:4 create_data.__init__ - A
    M 11:4 create_data.create3lvl_with_fake_customers - A
src/Customer/AllCustomers.py
    M 11:4 AllCustomers.create_customer_list - A
    M 29:4 AllCustomers.search_all_customers - A
    M 38:4 AllCustomers.getmaxid - A
    M 52:4 AllCustomers.getCustomerbyID - A
    C 5:0 AllCustomers - A
    M 6:4 AllCustomers.__init__ - A
    M 22:4 AllCustomers.arr_cust_to_dictarr - A
    M 61:4 AllCustomers.dict_name_id - A
    M 45:4 AllCustomers.addCustomer - A
    M 58:4 AllCustomers.getAllCustomers - A
src/Customer/Customer.py
    M 44:4 Customer.search - A
    M 54:4 Customer.setRegisterDate - A
    C 6:0 Customer - A
    M 8:4 Customer.__init__ - A
    M 21:4 Customer.update_email - A
    M 24:4 Customer.update_name - A
    M 27:4 Customer.get_all - A
    M 30:4 Customer.getEmail - A
    M 33:4 Customer.getName - A
    M 36:4 Customer.get_custId - A
    M 39:4 Customer.to_dict - A
    M 63:4 Customer.getRegisteredDate - A
    M 66:4 Customer.setID - A
    M 69:4 Customer.setBusinessName - A
    M 72:4 Customer.getBusinessName - A
    M 75:4 Customer.setPhone - A
    M 78:4 Customer.getPhone - A
    M 81:4 Customer.setFB - A
    M 84:4 Customer.getFB - A
    M 87:4 Customer.setTwitter - A
    M 90:4 Customer.getTwitter - A
    M 93:4 Customer.setLinkedin - A
    M 96:4 Customer.getLinkedin - A
src/Delivery/REST/rest_sad.py
    F 364:0 query_yes_no - B
    F 221:0 addCategory - B
    F 34:0 remove_data - A
    F 248:0 removeCategory - A
    F 271:0 editCategory - A
    F 297:0 editSortSettting - A
    F 83:0 getRevenuePastXMonths - A
    F 107:0 getNewCustomersPastXMonths - A
    F 197:0 addListing - A
    F 320:0 addAdvertiser - A
    F 340:0 editAdvertiser - A
    F 396:0 init_data - A
    F 140:0 getCustomerByID - A
    F 155:0 Search - A
    F 170:0 getSubCatPage - A
    F 185:0 getSubCatListing - A
    F 24:0 bad_request - A
    F 29:0 not_found - A
    F 56:0 hello_world - A
    F 60:0 getHomePage - A
    F 72:0 getCountofActiveListings - A
    F 131:0 getAllCustomers - A
src/Interactors/Reporting.py
    M 8:4 Reporting.NewCustomersMonths - A
    M 20:4 Reporting.getActiveListingHierchy - A
    M 43:4 Reporting.getActiveSubCatDict - A
    C 4:0 Reporting - A
    M 5:4 Reporting.__init__ - A
src/Interactors/Finance.py
    M 24:4 Finance.getIncomeMonths_category - B
    M 10:4 Finance.getIncomeMonths - A
    C 6:0 Finance - A
    M 44:4 Finance.isListingInRange - A
    M 7:4 Finance.__init__ - A
src/Sort/Sort.py
    M 15:4 SortListings.name - A
    M 48:4 SortListings.start_date - A
    C 5:0 SortListings - A
    M 7:4 SortListings.profitability - A
    M 11:4 SortListings.random - A
src/Actors/Users.py
    M 6:4 Users.getListingsByCategory - A
    C 2:0 Users - A
    M 3:4 Users.__init__ - A
src/Actors/CSR.py
    M 24:4 CSR.get_all_listing_for_customer - A
    C 2:0 CSR - A
    M 6:4 CSR.search_customers - A
    M 17:4 CSR.select_customer_id - A
    M 3:4 CSR.__init__ - A
    M 12:4 CSR.search - A
    M 32:4 CSR.getAllCustomers - A
src/Actors/Admin.py
    M 17:4 Admin.editnameSubCategory - A
    M 49:4 Admin.addListing - A
    M 9:4 Admin.addSubCategory - A
    M 32:4 Admin.deleteSubCategory - A
    C 3:0 Admin - A
    M 4:4 Admin.__init__ - A
    M 43:4 Admin.addCustomer - A
    M 59:4 Admin.getCustomer - A
    M 62:4 Admin.editSortSetting - A
src/Listing/Listing.py
    M 85:4 Listing.updateType - A
    M 32:4 Listing.changeStartDate - A
    M 40:4 Listing.setEndDate - A
    M 47:4 Listing.setCost - A
    M 112:4 Listing.isActive - A
    C 9:0 Listing - A
    M 10:4 Listing.__init__ - A
    M 23:4 Listing.set_init_date - A
    M 26:4 Listing.updateContent - A
    M 29:4 Listing.getContent - A
    M 55:4 Listing.getCost - A
    M 58:4 Listing.getStartDate - A
    M 61:4 Listing.getEndDate - A
    M 64:4 Listing.setListingID - A
    M 67:4 Listing.getListingID - A
    M 70:4 Listing.setImagePath - A
    M 73:4 Listing.getImagePath - A
    M 76:4 Listing.setWebsite - A
    M 79:4 Listing.getWebsite - A
    M 82:4 Listing.getType - A
    M 92:4 Listing.to_dict - A
    M 102:4 Listing.toString - A
    M 106:4 Listing.search - A
    M 109:4 Listing.getCustID - A
    M 118:4 Listing.setPath - A
    M 121:4 Listing.getPath - A
src/Categories/HomeCategory.py
    M 65:4 HomeCategory.traverse - A
    M 90:4 HomeCategory.getListingByCustomer - A
    M 17:4 HomeCategory.isSubCategoryValid - A
    M 51:4 HomeCategory.get_cat_featured - A
    M 31:4 HomeCategory.getActiveListing - A
    M 41:4 HomeCategory.depth_first_search_subcategories - A
    M 104:4 HomeCategory.getCategoryNames - A
    C 3:0 HomeCategory - A
    M 10:4 HomeCategory.getSubCategoryByName - A
    M 24:4 HomeCategory.addListing - A
    M 5:4 HomeCategory.__init__ - A
    M 38:4 HomeCategory.getActiveHomePageFeatured - A
    M 80:4 HomeCategory.addSubCategory - A
    M 87:4 HomeCategory.getAllSubCategories - A
src/Categories/SubCategory.py
    M 193:4 SubCategory.getListingCustomerID - B
    M 70:4 SubCategory.search_all - A
    M 122:4 SubCategory.traverse - A
    M 139:4 SubCategory.getmaxid - A
    M 151:4 SubCategory.getListingByID - A
    M 98:4 SubCategory.isSubCategoryValid - A
    M 105:4 SubCategory.get_cat_featured - A
    M 171:4 SubCategory.isCategoryEmpty - A
    M 35:4 SubCategory.addListing - A
    M 51:4 SubCategory.getActiveListing - A
    M 86:4 SubCategory.depth_first_search - A
    M 162:4 SubCategory.getCategoryNames - A
    M 184:4 SubCategory.removeSubCategory - A
    C 2:0 SubCategory - A
    M 25:4 SubCategory.getSubCategoryByName - A
    M 177:4 SubCategory.getSubCatIndex - A
    M 4:4 SubCategory.__init__ - A
    M 11:4 SubCategory.setName - A
    M 15:4 SubCategory.getName - A
    M 18:4 SubCategory.addSubCategory - A
    M 32:4 SubCategory.getAllSubCategories - A
    M 58:4 SubCategory.getActiveNormalListing - A
    M 61:4 SubCategory.getActiveFeaturedListing - A
    M 64:4 SubCategory.getAllNormalListing - A
    M 67:4 SubCategory.getAllFeaturedListing - A
src/Adapter/JsonToObjects.py
    C 6:0 JsonToObjects - C
    M 48:4 JsonToObjects.editCustomer - C
    M 86:4 JsonToObjects.addListing - C
    M 8:4 JsonToObjects.createCustomer - B

179 blocks (classes, functions, methods) analyzed.
Average complexity: A (2.346368715083799)


~~~
