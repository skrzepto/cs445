import sys
sys.path.append('/home/shims/Documents/cs445/')

import unittest2 as unittest
from src.Customer.AllCustomers import AllCustomers
from src.Customer.Customer import Customer

class TestAllCustomers(unittest.TestCase):


    def setUp(self):
        st = [(1, u'New name', u'New email2', 'Jun 10 2015  1:33PM'), (2, u'test name2', u'test2@gmail.com'), (3, u'test name3', u'test3@gmail.com')]
        self.allcust = AllCustomers(st)

    def tearDown(self):
        self.allcust = None

    def test_searchall_found_one(self):
        results = self.allcust.search_all_customers('name3')
        cid = results[0].get_custId()
        self.assertTrue(cid == 3 and len(results) ==1)

    def test_searchall_found_nothing(self):
        results = self.allcust.search_all_customers('name9')
        if not results:
            self.assertTrue(True)

    def test_customers_to_array_dict(self):
        arrdict = self.allcust.arr_cust_to_dictarr()
        self.assertEqual(arrdict[0]['name'], 'New name')

    def test_getmaxcustid(self):
        self.assertEqual(self.allcust.getmaxid(), 3)

    def test_addCustomer(self):
        # maxid = self.allcust.getmaxid() +1
        cust = Customer('Another Customer', 'another@customer.com')
        self.assertEqual(self.allcust.addCustomer(cust), cust)

    def test_getCustomerbyID_valid(self):
        self.assertIsNotNone(self.allcust.getCustomerbyID(1))

    def test_getCustomerbyID_invalid(self):
        self.assertIsNone(self.allcust.getCustomerbyID(100))

    def test_getCustomers(self):
        self.assertEqual(self.allcust.getAllCustomers(), self.allcust.customers)






if __name__ == '__main__':
    unittest.main()
