import datetime
import dateutil.relativedelta
import sys
sys.path.append('/home/shims/Documents/cs445/')

import unittest2 as unittest

from src.Customer.Customer import Customer


class TestCustomer(unittest.TestCase):

    def setUp(self):
        self.cust = Customer('some_name', 'email@test.com', 0)

    def tearDown(self):
        print('teardown')

    def test_todict(self):
        dt = self.cust.to_dict()
        exp = dt['name'] == 'some_name' and dt['email'] == 'email@test.com' and dt['cust_id'] == '0'
        self.assertTrue(exp)

    def test_getall(self):
        name, email, cust_id = self.cust.get_all()
        self.assertEqual(name, self.cust.getName())

    def test_updateName(self):
        self.cust.update_name('new_name')
        self.assertTrue(self.cust.getName()=='new_name')

    def test_updateEmail(self):
        self.cust.update_email('new_email@test.com')
        self.assertTrue(self.cust.getEmail()=='new_email@test.com')

    def test_getId(self):
        self.assertEqual(self.cust.get_custId(), 0)

    def test_search_true(self):
        self.assertTrue(self.cust.search('name'))

    def test_search_false(self):
        self.assertFalse(self.cust.search('what'))

    def test_getRegistrationDate(self):
        self.assertAlmostEqual(self.cust.getRegisteredDate(), datetime.datetime.now(), delta=datetime.timedelta(seconds=60))

    def test_setRegistrationDate_lastweek(self):
        d = datetime.datetime.now()
        d2 = d - dateutil.relativedelta.relativedelta(days=7)
        self.cust.setRegisterDate(d2)

        self.assertEqual(self.cust.getRegisteredDate(), d2)

    def test_setRegistrationDate_nextweek(self):
        d = datetime.datetime.now()
        d2 = d + dateutil.relativedelta.relativedelta(days=7)
        self.cust.setRegisterDate(d2)

        self.assertNotEqual(self.cust.getRegisteredDate(), d2)

    def test_setcustid(self):
        self.cust.setID(2)
        self.assertEqual(self.cust.get_custId(), 2)

    def test_setandget_BusName(self):
        self.cust.setBusinessName('name')
        self.assertEqual('name', self.cust.getBusinessName())

    def test_setandget_Phone(self):
        self.cust.setPhone('12345')
        self.assertEqual('12345', self.cust.getPhone())

    def test_setandget_FB(self):
        self.cust.setFB('fb.com/test')
        self.assertEqual(self.cust.getFB(), 'fb.com/test')

    def test_setandget_twitter(self):
        self.cust.setTwitter('twit.com/test')
        self.assertEqual(self.cust.getTwitter(), 'twit.com/test')

    def test_setandget_linkedin(self):
        self.cust.setLinkedin('linkedin.com/test')
        self.assertEqual(self.cust.getLinkedin(), 'linkedin.com/test')

if __name__ == '__main__':
    unittest.main()
