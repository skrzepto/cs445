from datetime import datetime

import dateutil.relativedelta


class Finance(object):
    def __init__(self):
        pass

    def getIncomeMonths(self, month, home_cat):
        income = 0
        home_feat = home_cat.getActiveHomePageFeatured()
        for l in home_feat:
            if self.isListingInRange(month, l):
                income = income + l.getCost()

        for sub_cat in home_cat.getAllSubCategories():
            for key in sub_cat:
                income = income+ self.getIncomeMonths_category(month, sub_cat[key])

        print(income)
        return income

    def getIncomeMonths_category(self, mon, subcat):
        income = 0
        normal_l = subcat.getAllNormalListing()
        feat_l = subcat.getAllFeaturedListing()

        for l in normal_l:
            if self.isListingInRange(mon, l):
                income = income + l.getCost()

        for l in feat_l:
            if self.isListingInRange(mon, l):
                income = income + l.getCost()

        for sub_cat in subcat.getAllSubCategories():
            for key in sub_cat:
                income = income+ self.getIncomeMonths_category(mon,sub_cat[key])

        return income


    def isListingInRange(self, mon, l):
        if datetime.now() >= l.getStartDate(): #has the listing ever been/or is active
                _startdate = l.getStartDate()
                print("Type: ",l.getType()," Start: ",  _startdate, " Cost: ", str(l.getCost()))

                delta = datetime.now() - dateutil.relativedelta.relativedelta(months=int(mon))
                if delta  <= _startdate:
                    return l

        return None