import json
from flask import Flask, jsonify, make_response, request

from src.Actors.Admin import Admin
from src.Actors.CSR import CSR
from src.Persistence.datashelve_t import datashelve_t
from src.Persistence.create_data import create_data
from src.Adapter.JsonToObjects import JsonToObjects
from src.sad import SAD
import os
import atexit

app = Flask(__name__)
env_setting = 'test'

"""API endpoint for submitting data to

    :return: status code 405 - invalid JSON or invalid request type
    :return: status code 400 - unsupported Content-Type or invalid publisher
    :return: status code 201 - successful submission
"""


@app.errorhandler(400)
def bad_request(error):
    return make_response(jsonify({'error': 'Bad request'}), 400)


@app.errorhandler(404)
def not_found(error):
    return make_response(jsonify({'error': 'Not found'}), 404)


def remove_data():
    try:
        os.remove('test_data')
    except:
        pass
    try:
        os.remove('test_data.db')
    except:
        pass

    try:
        os.remove('production_data')
    except:
        pass

    try:
        os.remove('production_data.db')
    except:
        pass



@app.route('/', methods=['GET'])
def hello_world():
    return 'Hello World!'

@app.route('/sad', methods=['GET'])
def getHomePage():
    db = datashelve_t(env_setting)
    sad = db.getData()
    print(sad)
    from src.Actors.Users import Users
    users = Users(sad)
    resp = users.getListingsByCategory([])
    print(resp)
    return jsonify(resp)


@app.route('/sad/report/count_active_listings', methods=['GET'])
def getCountofActiveListings():
    db = datashelve_t(env_setting)
    sad = db.getData()
    from src.Interactors.Reporting import Reporting
    report = Reporting()
    total, hierarchy = report.getActiveListingHierchy(sad.home_cat)
    resp = {'total_count': total, 'hierarchy': hierarchy}
    return make_response(jsonify(resp), 200)


@app.route('/sad/report/revenue', methods=['GET'])
def getRevenuePastXMonths():
    num_months = request.args.get('months')

    if num_months is not None:

        try:
            num_months = int(num_months)

        except ValueError:
            return make_response(jsonify({'Error': 'Must provide number of months to get revenue from listings'}),400)

        db = datashelve_t(env_setting)
        sad = db.getData()
        from src.Interactors.Finance import Finance
        report = Finance()
        res = report.getIncomeMonths(int(num_months), sad.home_cat)
        return make_response(jsonify({'Months': num_months, 'Revenue': res}), 200)


    else:
        return make_response(jsonify({'Error': 'key must be ?months=<int>'}), 400)


@app.route('/sad/report/new_customers', methods=['GET'])
def getNewCustomersPastXMonths():
    num_months = request.args.get('months')

    if num_months is not None:

        try:
            num_months = int(num_months)

        except ValueError:
            return make_response(jsonify({'Error': 'Must provide number of months to get revenue from listings'}), 400)

        db = datashelve_t(env_setting)
        sad = db.getData()
        from src.Interactors.Reporting import Reporting
        report = Reporting()
        res = report.NewCustomersMonths(num_months, sad.customers.customers)
        return make_response(jsonify({'Months': num_months, 'Customers': res}), 200)


    else:
        return make_response(jsonify({'Error': 'key must be ?months=<int>'}), 400)


@app.route('/sad/csr/advertisers', methods=['GET'])
def getAllCustomers():
    db = datashelve_t(env_setting)
    sad = db.getData()
    csr = CSR(sad)
    res = csr.getAllCustomers()
    return make_response(jsonify({'customers': res}), 200)


@app.route('/sad/csr/advertisers/<int:c_id>', methods=['GET'])
def getCustomerByID(c_id):
    db = datashelve_t(env_setting)
    sad = db.getData()
    csr = CSR(sad)
    res = csr.select_customer_id(c_id)

    if res:
        listings = csr.get_all_listing_for_customer(c_id)
        return make_response(jsonify({'customer': res, 'listings': listings}), 200)

    else:
        return make_response(jsonify({'customer': 'Not Found'}), 404)


@app.route('/sad/csr', methods=['GET'])
def Search():
    search_key = request.args.get('search')

    if search_key is not None:
        db = datashelve_t(env_setting)
        sad = db.getData()
        csr = CSR(sad)
        res = csr.search(search_key)
        return make_response(jsonify({'Search': search_key, 'Results': res}), 200)

    else:
        return make_response(jsonify({'Error': 'key must be ?search='}), 400)


@app.route('/sad/', defaults={'path': ''})
@app.route('/sad/<path:path>', methods=['GET'])
def getSubCatPage(path):
    db = datashelve_t(env_setting)
    sad = db.getData()
    print(sad)
    from src.Actors.Users import Users
    users = Users(sad)
    resp = users.getListingsByCategory(path.split('/'))
    print(resp)
    if resp:
        return jsonify(resp)
    else:
        return make_response(jsonify({'error': 'subcategory ' + str(path) + ' does not exist'}), 404)

@app.route('/sad/', defaults={'path': ''})
@app.route('/sad/<path:path>/<int:l_id>', methods=['GET'])
def getSubCatListing(path, l_id):
    db = datashelve_t(env_setting)
    sad = db.getData()
    arr_path = path.split('/')
    resp = sad.getListingbyPathID(arr_path, l_id)
    if resp:
        return jsonify(resp.to_dict())
    else:
        return make_response(jsonify({'error': 'Listing ' + str(l_id) + ' does not exist in ' + str(path)}), 404)

@app.route('/sad/admin/listing', methods=['POST'])
def addListing():

    db = datashelve_t(env_setting)
    sad = db.getData()

    try:
        requests = json.loads(request.data.decode())
        print(requests)
    except ValueError as e:
        return make_response(jsonify({'Error': 'invalid json'}), 400)

    adapter = JsonToObjects()
    new_l = adapter.addListing(requests)
    from src.Actors.Admin import Admin
    admin = Admin(sad)
    res = admin.addListing(new_l)
    if res:
        db.storeData(sad)
        return make_response(jsonify({'Listing_ID': str(res.getListingID()), 'Path': res.getPath()}), 200)

    return make_response(jsonify({'Error': 'invalid json unable to create a new listing'}), 400)


@app.route('/sad/admin/category/add', methods=['POST'])
def addCategory():
    db = datashelve_t(env_setting)
    sad = db.getData()

    try:
        requests = json.loads(request.data.decode())
        print(requests)
    except ValueError as e:
        return make_response(jsonify({'Error': 'invalid json'}), 400)

    if all(k in requests for k in ("Path", "SubCat_Name")):
        from src.Actors.Admin import Admin
        admin = Admin(sad)
        try:
            res = admin.addSubCategory(requests['Path'], requests['SubCat_Name'].lower())
        except:
            print("couldnt get values from json")
            return make_response(jsonify({'Error': 'invalid json unable to create a new category'}), 400)

        if res:
            db.storeData(sad)
            return make_response(jsonify({'Success': 'Created a new category named ' +
                                                     str(requests['SubCat_Name'].lower()) + ' under ' + "/".join(requests['Path'])}), 200)

    return make_response(jsonify({'Error': 'invalid json unable to create a new category'}), 400)

@app.route('/sad/admin/category/remove', methods=['DELETE'])
def removeCategory():
    db = datashelve_t(env_setting)
    sad = db.getData()

    try:
        requests = json.loads(request.data.decode())
        print(requests)
    except ValueError as e:
        return make_response(jsonify({'Error': 'invalid json'}), 400)

    try:
        path = requests['path']
    except:
        return make_response(jsonify({'Error': 'invalid json'}), 400)

    admin = Admin(sad)
    if admin.deleteSubCategory(path):
        db.storeData(sad)
        return make_response(jsonify({'Success': 'Deleted category' + '/'.join(path)}), 200)
    else:
        return make_response(jsonify({'Error': 'Unable to delete category either check json eg. {"path": ["sports"]} or the subcategory is not empty'}), 400)

@app.route('/sad/admin/category/edit', methods=['PUT'])
def editCategory():
    db = datashelve_t(env_setting)
    sad = db.getData()

    try:
        requests = json.loads(request.data.decode())
        print(requests)
    except ValueError as e:
        return make_response(jsonify({'Error': 'invalid json'}), 400)

    try:
        path = requests['path']
        new_name = str(requests['new_name'])
    except:
        return make_response(jsonify({'Error': 'invalid json'}), 400)

    admin = Admin(sad)

    if admin.editnameSubCategory(path, new_name):
        db.storeData(sad)
        return make_response(jsonify({'Success': 'Edited category name' + '/'.join(path) + ' to ' + new_name}), 200)
    else:
        return make_response(jsonify({'Error': 'Unable to edit category name either check json eg. {"path": ["sports"], "new_name": "athletics"} or the subcategory doesnt exist'}), 400)


@app.route('/sad/admin/sort_setting', methods=['PUT'])
def editSortSettting():
    db = datashelve_t(env_setting)
    sad = db.getData()

    try:
        requests = json.loads(request.data.decode())
        print(requests)
    except ValueError as e:
        return make_response(jsonify({'Error': 'invalid json'}), 400)

    try:
        setting = str(requests['setting'])
    except:
        return make_response(jsonify({'Error': 'invalid json'}), 400)

    admin = Admin(sad)
    if admin.editSortSetting(setting):
        db.storeData(sad)
        return make_response(jsonify({'Success': 'Edited the sort settings to' + setting}), 200)


    return make_response(jsonify({'Error': 'invalid json'}), 400)

@app.route('/sad/admin/advertiser', methods=['POST'])
def addAdvertiser():
    db = datashelve_t(env_setting)
    sad = db.getData()

    try:
        requests = json.loads(request.data.decode())
        print(requests)
    except ValueError as e:
        return make_response(jsonify({'Error': 'invalid json'}), 400)

    admin = Admin(sad)
    jstoob = JsonToObjects()
    cust = admin.addCustomer(jstoob.createCustomer(requests))
    if cust:
        db.storeData(sad)
        return make_response(jsonify({'Success': 'Created customer', 'info': cust.to_dict()}), 200)

    return make_response(jsonify({'Error': 'invalid json'}), 400)

@app.route('/sad/admin/advertiser', methods=['PUT'])
def editAdvertiser():
    db = datashelve_t(env_setting)
    sad = db.getData()

    try:
        requests = json.loads(request.data.decode())
        c_id = int(requests['customer_id'])
        print(requests)
    except ValueError as e:
        return make_response(jsonify({'Error': 'invalid json'}), 400)

    admin = Admin(sad)
    jstoob = JsonToObjects()
    original_cust = admin.getCustomer(c_id)
    edited_cust = jstoob.editCustomer(requests, original_cust)
    if edited_cust:
        db.storeData(sad)
        return make_response(jsonify({'Success': 'Edited customer', 'info': edited_cust.to_dict()}), 200)

    return make_response(jsonify({'Error': 'invalid json'}), 400)

import sys

def query_yes_no(question, default="yes"):
    """Ask a yes/no question via raw_input() and return their answer.

    "question" is a string that is presented to the user.
    "default" is the presumed answer if the user just hits <Enter>.
        It must be "yes" (the default), "no" or None (meaning
        an answer is required of the user).

    The "answer" return value is True for "yes" or False for "no".
    """
    valid = {"yes": True, "y": True, "ye": True,
             "no": False, "n": False}
    if default is None:
        prompt = " [y/n] "
    elif default == "yes":
        prompt = " [Y/n] "
    elif default == "no":
        prompt = " [y/N] "
    else:
        raise ValueError("invalid default answer: '%s'" % default)

    while True:
        sys.stdout.write(question + prompt)
        choice = input().lower()
        if default is not None and choice == '':
            return valid[default]
        elif choice in valid:
            return valid[choice]
        else:
            sys.stdout.write("Please respond with 'yes' or 'no' "
                             "(or 'y' or 'n').\n")

def init_data(env_setting, blank):
    if env_setting in ['test', 'production']:

        if blank:
            sad = SAD()
            db = datashelve_t(env_setting).storeData(sad)

        else:
            sad = create_data().create3lvl_with_fake_customers()
            db = datashelve_t(env_setting).storeData(sad)


if __name__ == '__main__':
    atexit.register(remove_data)
    try:
        script, env_setting = sys.argv
    except:
        env_setting = 'test'

    finally:

        if env_setting == 'test':
            init_data('test', False)
            print(env_setting + ' local debug is true and running on localhost')
            app.run(debug=True)

        elif env_setting == 'production':

            if query_yes_no("Do you want a blank data structure"):
                init_data('production', True)
            else:
                init_data('production', False)

            print(env_setting + ' the world can see')
            app.run(host='0.0.0.0')  # to be visible online

        else:
            print("something failed")





