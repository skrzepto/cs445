import unittest2 as unittest

from src.Customer.AllCustomers import AllCustomers
from src.sad import SAD
from src.Listing.Listing import Listing
from datetime import datetime
import dateutil.relativedelta

import unittest.mock

class testSAD(unittest.TestCase):
    def setUp(self):
        self.s = SAD()
        self.home_cat = self.s.home_cat
        self.maxDiff = None

    def test_getLisitngPathandID(self):
        self.create3lvl()
        l = self.s.getListingbyPathID(['sports', 'soccer', 'gear'], 1)
        print(l)
        self.assertIsNotNone(l)

    def test_getListingPath_random(self):
        self.create3lvl()
        list_arr = self.s.getListingbyPath(['sports', 'soccer', 'gear'])
        print(list_arr)

        d_now = datetime.now()
        d_nextmonth = d_now + dateutil.relativedelta.relativedelta(months=1)
        d_lastweek = d_now - dateutil.relativedelta.relativedelta(days=7)

        expected = {'sports/soccer/gear': {'normal': [], 'category-featured': [
            {'cost': 100, 'type': 'category-featured', 'cust_id': 0,
             'end_date': d_nextmonth.strftime('%b %d %Y %I:%M%p'),
             'start_date': d_lastweek.strftime('%b %d %Y %I:%M%p'),
             'content': 'waldo is in sports featured',
             'listing_id': 1, 'image': 'path/to/image', 'path': [],
             'profit': 2.7027027027027026,'website': 'somewebsite.com', 'active': True}], 'sub-categories': []}}
        self.assertDictEqual(list_arr, expected)

    def test_getListingPath_date(self):
        self.create3lvl()
        self.s.editSortSetting('most-recent')

        list_arr = self.s.getListingbyPath(['sports', 'soccer', 'gear'])
        print(list_arr)

        d_now = datetime.now()
        d_nextmonth = d_now + dateutil.relativedelta.relativedelta(months=1)
        d_lastweek = d_now - dateutil.relativedelta.relativedelta(days=7)

        expected = {'sports/soccer/gear': {'normal': [], 'category-featured': [
            {'cost': 100, 'type': 'category-featured', 'cust_id': 0,
             'end_date': d_nextmonth.strftime('%b %d %Y %I:%M%p'),
             'start_date': d_lastweek.strftime('%b %d %Y %I:%M%p'),
             'content': 'waldo is in sports featured',
             'listing_id': 1, 'image': 'path/to/image', 'path': [],
             'profit': 2.7027027027027026,'website': 'somewebsite.com', 'active': True}], 'sub-categories': []}}
        self.assertDictEqual(list_arr, expected)

    def test_getListingPath_profitable(self):
        self.create3lvl()
        self.s.editSortSetting('most-profitable')

        list_arr = self.s.getListingbyPath(['sports', 'soccer', 'gear'])
        print(list_arr)

        d_now = datetime.now()
        d_nextmonth = d_now + dateutil.relativedelta.relativedelta(months=1)
        d_lastweek = d_now - dateutil.relativedelta.relativedelta(days=7)

        expected = {'sports/soccer/gear': {'normal': [], 'category-featured': [
            {'cost': 100, 'type': 'category-featured', 'cust_id': 0,
             'end_date': d_nextmonth.strftime('%b %d %Y %I:%M%p'),
             'start_date': d_lastweek.strftime('%b %d %Y %I:%M%p'),
             'content': 'waldo is in sports featured',
             'listing_id': 1, 'image': 'path/to/image', 'path': [],
             'profit': 2.7027027027027026,'website': 'somewebsite.com', 'active': True}], 'sub-categories': []}}
        self.assertDictEqual(list_arr, expected)

    def test_getListingPath_name(self):
        self.create3lvl()
        self.s.customers = self.create_fake_customers()
        self.s.editSortSetting('advertiser-name')

        list_arr = self.s.getListingbyPath(['sports', 'soccer', 'gear'])
        print(list_arr)

        d_now = datetime.now()
        d_nextmonth = d_now + dateutil.relativedelta.relativedelta(months=1)
        d_lastweek = d_now - dateutil.relativedelta.relativedelta(days=7)

        expected = {'sports/soccer/gear': {'normal': [], 'category-featured': [
            {'cost': 100, 'type': 'category-featured', 'cust_id': 0,
             'end_date': d_nextmonth.strftime('%b %d %Y %I:%M%p'),
             'start_date': d_lastweek.strftime('%b %d %Y %I:%M%p'),
             'content': 'waldo is in sports featured',
             'listing_id': 1, 'image': 'path/to/image', 'path': [],
             'profit': 2.7027027027027026,'website': 'somewebsite.com', 'active': True}], 'sub-categories': []}}
        self.assertDictEqual(list_arr, expected)


    def test_getLisitngPath_nopath(self):
        list_arr = self.s.getListingbyPath(['none', 'soccer', 'gear'])
        self.assertIsNone(list_arr)

    def test_getLisitngPathandID_nopath(self):
        lis = self.s.getListingbyPathID(['none', 'soccer', 'gear'], 9999)
        self.assertIsNone(lis)

    def test_search_empty(self):
        srch_res = self.s.search('nothing')
        self.assertDictEqual(srch_res, {'listings': [], 'customers': []})

    def test_search_find_waldo(self):
        self.create3lvl()
        self.s.customers = self.create_fake_customers()
        srch_res = self.s.search('waldo')
        print(srch_res)
        self.assertEqual(len(srch_res['listings']), 4)

    def test_getcustomerbyid_valid(self):
        self.s.customers = self.create_fake_customers()
        # print(self.s.customers.arr_cust_to_dictarr(self.s.customers.customers))
        self.assertIsNotNone(self.s.getAdvertiserbyID(1))

    def test_getcustomerbyid_notvalid(self):
        self.s.customers = self.create_fake_customers()
        # print(self.s.customers.arr_cust_to_dictarr(self.s.customers.customers))
        self.assertIsNone(self.s.getAdvertiserbyID(9999))

    def test_getAllAdvirtisers_in_dictformat(self):
        self.s.customers = self.create_fake_customers()
        res = self.s.getAllAdvertiser()
        print(res)
        self.assertEqual(len(res), 4)

    def test_addlisting_valid(self):
        self.s.customers = self.create_fake_customers()
        self.create3lvl()
        d_now = datetime.now()

        d_lastweek = d_now - dateutil.relativedelta.relativedelta(days=7)
        d_nextmonth = d_now + dateutil.relativedelta.relativedelta(months=1)

        new_l = Listing(2)
        new_l.set_init_date(d_lastweek)
        new_l.setEndDate(d_nextmonth)
        new_l.updateContent('test adding a customer')
        new_l.updateType('category-featured')
        new_l.setCost(400)

        self.assertEqual(self.s.addListingbyPath(['sports', 'soccer'], new_l), 2)  # listing ID is returned

    def test_addlisting_valid_2(self):
        self.s.customers = self.create_fake_customers()
        self.create3lvl()
        d_now = datetime.now()

        d_lastweek = d_now - dateutil.relativedelta.relativedelta(days=7)
        d_nextmonth = d_now + dateutil.relativedelta.relativedelta(months=1)

        new_l = Listing(29)
        new_l.set_init_date(d_lastweek)
        new_l.setEndDate(d_nextmonth)
        new_l.updateContent('test adding a customer')
        new_l.updateType('category-featured')
        new_l.setCost(400)
        # customer id is not in the customer array
        res = self.s.addListingbyPath(['sports', 'soccer'], new_l)
        self.assertIsNone(res) # listing ID is returned

    def test_addlisting_notvalid(self):
        self.s.customers = self.create_fake_customers()
        self.create3lvl()
        d_now = datetime.now()

        d_lastweek = d_now - dateutil.relativedelta.relativedelta(days=7)
        d_nextmonth = d_now + dateutil.relativedelta.relativedelta(months=1)

        new_l = Listing(2)
        new_l.set_init_date(d_lastweek)
        new_l.setEndDate(d_nextmonth)
        new_l.updateContent('test adding a listing')
        new_l.updateType('category-featured')
        new_l.setCost(400)

        self.assertIsNone(self.s.addListingbyPath(['not', 'valid'], new_l))  # listing ID is returned

    def test_getListingforCustomer_happy(self):
        self.create3lvl()
        res = self.s.getListingByCustomer(0)
        # better to count the amount of listings since the times change for the test data
        self.assertEqual(len(res), 5)

    def test_getListingforCustomer_nolistings(self):
        res = self.s.getListingByCustomer(0)
        self.assertEqual(res, [])

    def test_traverse_valid(self):
        self.create3lvl()
        sub_cat = self.s.traverse(['sports', 'soccer'])
        self.assertIsNotNone(sub_cat, 'subcategory soccer exists')

    def test_traverse_notvalid(self):
        sub_cat = self.s.traverse(['sports'])
        self.assertIsNone(sub_cat, 'No categories are created yet')

    def test_gethomecategory(self):
        res = self.s.getListingbyPath([])
        self.assertIsNotNone(res)

    def test_editSortSetting_valid(self):
        self.assertEqual(self.s.editSortSetting('random'), 'random')

    def test_editSortSetting_notvalid(self):
        self.assertIsNone(self.s.editSortSetting('garbage'))

    def create_fake_customers(self):
        d_now = datetime.now()
        d_lastweek = d_now - dateutil.relativedelta.relativedelta(days=7)
        d_lastmonth = d_now - dateutil.relativedelta.relativedelta(months=1)
        d_last8months = d_now - dateutil.relativedelta.relativedelta(months=8)
        d_last5months = d_now - dateutil.relativedelta.relativedelta(months=5)

        st = [(1, u'New name', u'New email2', d_last5months.strftime('%b %d %Y %I:%M%p')),
              (2, u'test name2', u'test2@gmail.com', d_lastmonth.strftime('%b %d %Y %I:%M%p')),
              (3, u'test name3', u'test3@gmail.com', d_last8months.strftime('%b %d %Y %I:%M%p')),
              (0, u'test name3', u'test3@gmail.com', d_last8months.strftime('%b %d %Y %I:%M%p'))]
        allcust = AllCustomers(st)
        return allcust

    def create3lvl(self):
        d_now = datetime.now()
        d_yesterday = d_now - dateutil.relativedelta.relativedelta(days=1)
        d_lastweek = d_now - dateutil.relativedelta.relativedelta(days=7)
        d_lastmonth = d_now - dateutil.relativedelta.relativedelta(months=1)
        d_last2months = d_now - dateutil.relativedelta.relativedelta(months=2)
        d_last5months = d_now - dateutil.relativedelta.relativedelta(months=5)

        d_tommorow = d_now + dateutil.relativedelta.relativedelta(days=1)
        d_nextweek = d_now + dateutil.relativedelta.relativedelta(days=7)
        d_nextmonth = d_now + dateutil.relativedelta.relativedelta(months=1)

        l_home = Listing(0, 1)
        l_home.updateType('home-page-featured')
        l_home.updateContent('This is a home page listing')
        l_home.setCost(2500)
        l_home.set_init_date(d_yesterday)
        l_home.setEndDate(d_nextmonth)
        self.home_cat.addListing(l_home)

        l = Listing(0)
        l.set_init_date(d_lastmonth)
        l.setEndDate(d_nextweek)
        l.updateContent('where is waldo in sports: normal')
        l.setCost(300)

        l_1 = Listing(0)
        l_1.set_init_date(d_last5months)
        l_1.setEndDate(d_last2months)
        l_1.updateContent('where is waldo in sports: category featured')
        l_1.updateType('category-featured')
        l_1.setCost(400)

        cat_sports = self.home_cat.addSubCategory('sports')
        cat_sports['sports'].addListing(l)
        cat_sports['sports'].addListing(l_1)

        l2 = Listing(0)
        l2.set_init_date(d_last2months)
        l2.setEndDate(d_nextmonth)
        l2.setCost(250)
        l2.updateContent('waldo is in sports featured')

        cat_soccer = cat_sports['sports'].addSubCategory('soccer')
        cat_soccer['soccer'].addListing(l2)

        l3 = Listing(0)
        l3.set_init_date(d_lastweek)
        l3.setEndDate(d_nextmonth)
        l3.updateType('category-featured')
        l3.updateContent('waldo is in sports featured')

        cat_gear = cat_soccer['soccer'].addSubCategory('gear')
        cat_gear['gear'].addListing(l3)

        print(self.home_cat)


if __name__ == '__main__':
    unittest.main()
