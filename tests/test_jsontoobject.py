import dateutil.relativedelta
from src.Customer.Customer import Customer
from src.Adapter.JsonToObjects import JsonToObjects
import unittest2 as unittest
import datetime


class test_JsonToObjects(unittest.TestCase):
    print('JsonToObjects')

    def setUp(self):
        self.jstoob = JsonToObjects()

    def test_CreatCustomer_minimal_valid(self):
        json_data = {'name': 'New customer Name', 'email': 'test@email.com'}

        new_cust = self.jstoob.createCustomer(json_data)

        expected_data = {'business': 'business name', 'cust_id': '0',
                         'email': 'test@email.com', 'facebook': 'fb.com/user',
                         'linkedin': 'linkedin.com/user', 'name': 'New customer Name',
                         'phone': '8675309', 'registered': datetime.datetime.now().strftime('%b %d %Y %I:%M%p'),
                         'twitter': 'twitter.com/user'}

        self.assertDictEqual(expected_data, new_cust.to_dict(), 'customer object was created as expected')

    def test_CreatCustomer_comprehensive_valid(self):
        json_data = {'name': 'New customer Name', 'email': 'test@email.com',
                     'business_name': 'Apple', 'fb': 'myfb.com', 'linkedin': 'newlink',
                     'phone': '123456', 'twitter': 'mytwit'}

        new_cust = self.jstoob.createCustomer(json_data)

        expected_data = {'business': 'Apple', 'cust_id': '0',
                         'email': 'test@email.com', 'facebook': 'myfb.com',
                         'linkedin': 'newlink', 'name': 'New customer Name',
                         'phone': '123456', 'registered': datetime.datetime.now().strftime('%b %d %Y %I:%M%p'),
                         'twitter': 'mytwit'}

        self.assertDictEqual(expected_data, new_cust.to_dict(), 'customer object was created as expected')

    def test_CreatCustomer_minimal_datechange_valid(self):
        d_lastmonth = datetime.datetime.now() - dateutil.relativedelta.relativedelta(months=1)

        json_data = {'name': 'New customer Name', 'email': 'test@email.com',
                     'registration_date': d_lastmonth.strftime('%b %d %Y %I:%M%p')}

        new_cust = self.jstoob.createCustomer(json_data)

        expected_data = {'business': 'business name', 'cust_id': '0',
                         'email': 'test@email.com', 'facebook': 'fb.com/user',
                         'linkedin': 'linkedin.com/user', 'name': 'New customer Name',
                         'phone': '8675309', 'registered': d_lastmonth.strftime('%b %d %Y %I:%M%p'),
                         'twitter': 'twitter.com/user'}

        self.assertDictEqual(expected_data, new_cust.to_dict(), 'customer object was created as expected')

    def test_CreatCustomer_minimal_datechange_notvalid(self):
        json_data = {'name': 'New customer Name', 'email': 'test@email.com',
                     'registration_date': 'invalid date passed in'}

        new_cust = self.jstoob.createCustomer(json_data)

        expected_data = {'business': 'business name', 'cust_id': '0',
                         'email': 'test@email.com', 'facebook': 'fb.com/user',
                         'linkedin': 'linkedin.com/user', 'name': 'New customer Name',
                         'phone': '8675309', 'registered': datetime.datetime.now().strftime('%b %d %Y %I:%M%p'),
                         'twitter': 'twitter.com/user'}

        self.assertDictEqual(expected_data, new_cust.to_dict(),
                             'customer object was created with an invlaid date passed in handled by my code')

    def test_CreateCustomer_garbage_data(self):
        json_data = {'garbagein': 'garbageout'}
        new_cust = self.jstoob.createCustomer(json_data)
        self.assertIsNone(new_cust)

    def test_CreateCustomer_minimal_valid_with_garbage(self):
        json_data = {'name': 'New customer Name', 'email': 'test@email.com', 'garbage': 'data'}

        new_cust = self.jstoob.createCustomer(json_data)

        expected_data = {'business': 'business name', 'cust_id': '0',
                         'email': 'test@email.com', 'facebook': 'fb.com/user',
                         'linkedin': 'linkedin.com/user', 'name': 'New customer Name',
                         'phone': '8675309', 'registered': datetime.datetime.now().strftime('%b %d %Y %I:%M%p'),
                         'twitter': 'twitter.com/user'}

        self.assertDictEqual(expected_data, new_cust.to_dict(), 'customer object was created as expected')

    def test_EditCustomer_minimal_valid(self):
        json_data = {'customer_id': 1, 'name': 'New Name'}
        orig_cust = Customer('Original Name', 'old@email.com', 1)
        edited_cust = self.jstoob.editCustomer(json_data, orig_cust)

        expected_data = {'business': 'business name', 'cust_id': '1',
                         'email': 'old@email.com', 'facebook': 'fb.com/user',
                         'linkedin': 'linkedin.com/user', 'name': 'New Name',
                         'phone': '8675309', 'registered': datetime.datetime.now().strftime('%b %d %Y %I:%M%p'),
                         'twitter': 'twitter.com/user'}

        self.assertDictEqual(expected_data, edited_cust.to_dict(), 'editing a customer with minimal values didnt work')

    def test_EditCustomer_all_valid(self):
        d_lastmonth = datetime.datetime.now() - dateutil.relativedelta.relativedelta(months=1)

        json_data = {'customer_id': 1, 'name': 'New Name', 'business_name': 'new bus name',
                     'registration_date': d_lastmonth.strftime('%b %d %Y %I:%M%p'), 'phone': '1234', 'fb': 'myfb.com',
                     'twitter': 'mytwit.com', 'linkedin': 'newlink.com',
                     'email': 'new@gmail.com'}
        orig_cust = Customer('Original Name', 'old@email.com', 1)
        edited_cust = self.jstoob.editCustomer(json_data, orig_cust)

        expected_data = {'business': 'new bus name', 'cust_id': '1',
                         'email': 'new@gmail.com', 'facebook': 'myfb.com',
                         'linkedin': 'newlink.com', 'name': 'New Name',
                         'phone': '1234', 'registered': d_lastmonth.strftime('%b %d %Y %I:%M%p'),
                         'twitter': 'mytwit.com'}

        self.assertDictEqual(expected_data, edited_cust.to_dict(), 'editing a customer with minimal values didnt work')

    def test_EditCustomer_all_with_bad_date(self):
        d_lastmonth = datetime.datetime.now() - dateutil.relativedelta.relativedelta(months=1)

        json_data = {'customer_id': 1, 'name': 'New Name', 'business_name': 'new bus name',
                     'registration_date': 'bad date', 'phone': '1234', 'fb': 'myfb.com',
                     'twitter': 'mytwit.com', 'linkedin': 'newlink.com',
                     'email': 'new@gmail.com', }
        orig_cust = Customer('Original Name', 'old@email.com', 1)
        edited_cust = self.jstoob.editCustomer(json_data, orig_cust)

        expected_data = {'business': 'new bus name', 'cust_id': '1',
                         'email': 'new@gmail.com', 'facebook': 'myfb.com',
                         'linkedin': 'newlink.com', 'name': 'New Name',
                         'phone': '1234', 'registered': datetime.datetime.now().strftime('%b %d %Y %I:%M%p'),
                         'twitter': 'mytwit.com'}

        self.assertDictEqual(expected_data, edited_cust.to_dict(), 'editing a customer with minimal values didnt work')

    def test_EditCustomer_no_customerID(self):
        d_lastmonth = datetime.datetime.now() - dateutil.relativedelta.relativedelta(months=1)

        json_data = {'name': 'New Name', 'business_name': 'new bus name',
                     'registration_date': 'bad date', 'phone': '1234', 'fb': 'myfb.com',
                     'twitter': 'mytwit.com', 'linkedin': 'newlink.com',
                     'email': 'new@gmail.com', }
        orig_cust = Customer('Original Name', 'old@email.com', 1)
        edited_cust = self.jstoob.editCustomer(json_data, orig_cust)

        self.assertIsNone(edited_cust)

    def test_addListing_minimal_valid(self):
        # "customer_id","start_date", "end_date", "content", "cost", "path"
        d_lastmonth = datetime.datetime.now() - dateutil.relativedelta.relativedelta(months=1)
        d_nextmonth = datetime.datetime.now() + dateutil.relativedelta.relativedelta(months=1)

        json_data = {'customer_id': 1, 'start_date': d_lastmonth.strftime('%b %d %Y %I:%M%p'), 'end_date': d_nextmonth.strftime('%b %d %Y %I:%M%p'),
                     'path': ['soccer'], 'content': 'descrive the ad', 'cost': 400}

        expected = {'cust_id': 1, 'start_date': d_lastmonth.strftime('%b %d %Y %I:%M%p'),
                    'end_date': d_nextmonth.strftime('%b %d %Y %I:%M%p'), 'path': ['soccer'],
                    'type': 'normal', 'website': 'somewebsite.com', 'profit': 6.557377049180328,
                    'image': 'path/to/image', 'listing_id': 0, 'active': True,
                    'content': 'descrive the ad', 'cost': 400}

        new_l = self.jstoob.addListing(json_data)
        self.assertDictEqual(expected, new_l.to_dict())

    def test_addListing_garbage_custid(self):
        # "customer_id","start_date", "end_date", "content", "cost", "path"
        d_lastmonth = datetime.datetime.now() - dateutil.relativedelta.relativedelta(months=1)
        d_nextmonth = datetime.datetime.now() + dateutil.relativedelta.relativedelta(months=1)

        json_data = {'customer_id': '1a', 'start_date': d_lastmonth.strftime('%b %d %Y %I:%M%p'), 'end_date': d_nextmonth.strftime('%b %d %Y %I:%M%p'),
                     'path': ['soccer'], 'content': 'descrive the ad', 'cost': 400}

        new_l = self.jstoob.addListing(json_data)
        self.assertIsNone(new_l)

    def test_addListing_garbage_dates(self):
        # "customer_id","start_date", "end_date", "content", "cost", "path"
        d_lastmonth = datetime.datetime.now() - dateutil.relativedelta.relativedelta(months=1)
        d_nextmonth = datetime.datetime.now() + dateutil.relativedelta.relativedelta(months=1)

        json_data = {'customer_id': 1, 'start_date': 'garbage date', 'end_date': d_nextmonth.strftime('%b %d %Y %I:%M%p'),
                     'path': ['soccer'], 'content': 'descrive the ad', 'cost': 400}

        new_l = self.jstoob.addListing(json_data)
        self.assertIsNone(new_l)

    def test_addListing_nodata(self):
        # "customer_id","start_date", "end_date", "content", "cost", "path"
        d_lastmonth = datetime.datetime.now() - dateutil.relativedelta.relativedelta(months=1)
        d_nextmonth = datetime.datetime.now() + dateutil.relativedelta.relativedelta(months=1)

        json_data = {'test': 'bad'}

        new_l = self.jstoob.addListing(json_data)
        self.assertIsNone(new_l)


    def test_addListing_all_valid(self):
        # "customer_id","start_date", "end_date", "content", "cost", "path"
        d_lastmonth = datetime.datetime.now() - dateutil.relativedelta.relativedelta(months=1)
        d_nextmonth = datetime.datetime.now() + dateutil.relativedelta.relativedelta(months=1)

        json_data = {'customer_id': 1, 'start_date': d_lastmonth.strftime('%b %d %Y %I:%M%p'), 'end_date': d_nextmonth.strftime('%b %d %Y %I:%M%p'),
                     'path': ['soccer'], 'content': 'descrive the ad', 'cost': 400,
                     'type': 'category-featured', 'website': 'web.com', 'image': 'img'}

        expected = {'cust_id': 1, 'start_date': d_lastmonth.strftime('%b %d %Y %I:%M%p'),
                    'end_date': d_nextmonth.strftime('%b %d %Y %I:%M%p'), 'path': ['soccer'],
                    'type': 'category-featured', 'website': 'web.com', 'profit': 6.557377049180328,
                    'image': 'img', 'listing_id': 0, 'active': True,
                    'content': 'descrive the ad', 'cost': 400}

        new_l = self.jstoob.addListing(json_data)
        self.assertDictEqual(expected, new_l.to_dict())

    def test_addListing_minimal_enddate_before_start(self):
        # "customer_id","start_date", "end_date", "content", "cost", "path"
        d_lastmonth = datetime.datetime.now() - dateutil.relativedelta.relativedelta(months=1)
        d_nextmonth = datetime.datetime.now() + dateutil.relativedelta.relativedelta(months=1)

        json_data = {'customer_id': 1, 'start_date': d_nextmonth.strftime('%b %d %Y %I:%M%p'), 'end_date': d_lastmonth.strftime('%b %d %Y %I:%M%p'),
                     'path': ['soccer'], 'content': 'descrive the ad', 'cost': 400}

        new_l = self.jstoob.addListing(json_data)
        self.assertIsNone(new_l)

    def test_addListing_cost_invalid(self):
        # "customer_id","start_date", "end_date", "content", "cost", "path"
        d_lastmonth = datetime.datetime.now() - dateutil.relativedelta.relativedelta(months=1)
        d_nextmonth = datetime.datetime.now() + dateutil.relativedelta.relativedelta(months=1)

        json_data = {'customer_id': 1, 'start_date': d_lastmonth.strftime('%b %d %Y %I:%M%p'), 'end_date': d_nextmonth.strftime('%b %d %Y %I:%M%p'),
                     'path': ['soccer'], 'content': 'descrive the ad', 'cost': -400,
                     'type': 'category-featured', 'website': 'web.com', 'image': 'img'}

        new_l = self.jstoob.addListing(json_data)
        self.assertIsNone(new_l)

    def test_addListing_cost_stringwithint(self):
        # "customer_id","start_date", "end_date", "content", "cost", "path"
        d_lastmonth = datetime.datetime.now() - dateutil.relativedelta.relativedelta(months=1)
        d_nextmonth = datetime.datetime.now() + dateutil.relativedelta.relativedelta(months=1)

        json_data = {'customer_id': 1, 'start_date': d_lastmonth.strftime('%b %d %Y %I:%M%p'), 'end_date': d_nextmonth.strftime('%b %d %Y %I:%M%p'),
                     'path': ['soccer'], 'content': 'descrive the ad', 'cost': '$400',
                     'type': 'category-featured', 'website': 'web.com', 'image': 'img'}

        new_l = self.jstoob.addListing(json_data)
        self.assertIsNone(new_l)


if __name__ == '__main__':
    unittest.main()
