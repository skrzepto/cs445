from datetime import datetime
import dateutil.relativedelta

class Reporting():
    def __init__(self):
        pass

    def NewCustomersMonths(self, mon, all_customers):
        d_now = datetime.now()
        d_delta = d_now - dateutil.relativedelta.relativedelta(months=int(mon))

        result = []

        for cus in all_customers:
            if cus.getRegisteredDate() >= d_delta:
                result.append(cus.to_dict())

        return result

    def getActiveListingHierchy(self, home_cat):
        '''
        sad = {'home': {featured: 1,
                        subcategories: [{'sports': {cat_feat: 2,
                                                    normal 4,
                                                    subcategories: []}]}
        total = 7
        '''
        total_count = 0
        total_home = len(home_cat.getActiveHomePageFeatured())
        total_count += total_home
        sub_arr = []

        for other_sub_cat in home_cat.getAllSubCategories():
            for key in other_sub_cat:
                new_arr_sub, c = self.getActiveSubCatDict(other_sub_cat[key])
                sub_arr.append(new_arr_sub)
                total_count += c

        sad_hierchy = {'home-featured': total_home, 'sub': sub_arr}
        print(sad_hierchy)
        return total_count, sad_hierchy

    def getActiveSubCatDict(self, sub_cat):
        total_count = 0

        total_normal = len(sub_cat.getActiveNormalListing())
        total_feat = len(sub_cat.getActiveFeaturedListing())

        sub_arr = []

        total_count = total_count + total_normal + total_feat

        for other_sub_cat in sub_cat.getAllSubCategories():
            for key in other_sub_cat:
                sub_arr_tmp, c = self.getActiveSubCatDict(other_sub_cat[key])
                sub_arr.append(sub_arr_tmp)
                total_count = total_count + c

        result = {str(sub_cat['name']): {'normal': total_normal, 'category-featured': total_feat, 'sub': sub_arr}}

        print(result)

        return result, total_count

