REPO: https://bitbucket.org/skrzepto/cs445

**Status**: Complete

**Code Coverage**: 85% statement coverage

**Lines of Code**: ~3,000 *(1,300 app, 1700 tests)*

**Purpose**: Academic Project CS 445

**What is it**: Online yellow pages clone using REST as the backend

    Important information: Focus of project is backend and the front end is extra credit. Data validation
    is not needed but i'll do it because it doesn't take too much work. Databases are
    banned from this project so I'll be persisting data using python shelve.




#How to deploy#
Assuming our deployment platform is Ubuntu 14.04lts as specified by the professor


####*NOTES BEFORE DEPLOYING*#####
1. my unittests are time relevant (eg. variables named yesterday, tommorrow, ect...)
and if the tests are ran at 2:54:55 pm it'll fail since the next time in middle of execution is 2:55:00

	Solution: rerun the tests again please

2. All unittests on a fast machine execute within 3-5seconds, on slower machines its 6-10seconds
Reason is I have fairly comprehensive tests and didn't mock anything out

3. make sure to enter sudo password when prompted to install dependencies
4. if the unittests completely fail please execute the following line
~~~
source ~/.basrc
~~~
and rerun ~/.setup.sh without installing dependencies

### Steps to deployment ###
1. The installer will ask you if you'd like to install dependencies and pull the project so hit 'y' once.
2. Then it'll ask you if the unittests pass hit 'y' then 'enter'.
3. Afterwards it'll ask if you want production or tests. Press '1' then 'enter' to deploy to production
4. Finally, it'll ask if you want empty data strucutre hit 'y' or 'n' then 'enter'


###Commands to Run###
```
wget -c -O ~/setup.sh https://bitbucket.org/skrzepto/cs445/raw/master/setup.sh
sudo chmod +x ~/setup.sh
~/setup.sh
```

----------------------------------------------------
#USE CASES#
##CSR##
- search: advertiser
- select advertiser by id
- return info for advertiser
- search all listings by advertiser 
- get listings for advertiser id
- search/look up all listing past present or future

##Admin##
- Sort: set the global settings
	- featured listings (home, category)
- Categories
    -     add
    -     modify/update
    -     remove
		Can’t remove categories with listings in its subcategories
- Advertiser
    - add
    - edit
-  Listings
	- add


##User##
- traverse categories
- no category specified go to home page

##Report##
- show active listings count with its type and hierarchy
- get customers created in x months
- money made x months


------------------------------------------------------------------------------------------------------------------------

#Attributes#

##Advertiser##
customer id
registration date
name
email
name of business
phone
fb
twitter
linkedin

##Listing##
start date
end date
listing id
customer id
type: regular, home-featured, featured-category
website
image
price
content/description

##Category##
name of category
sub categories
listings array


------------------------------------------------------------------------------------------------------------------------



#REST#


##Implemented##
- GET */sad*
	- home page featured and subcategories should be shown

- GET */sad/sports*
	- we only see sports category featured sorted by global setting and its normal listings in any order
	- and we see sports subcategories

- GET */sad/sports/soccer*
	- we see sports and soccer category featured sorted by global setting and its normal listings in any order and we see soccer subcategories

- GET */sad/report/count_active_listings*
	- Get all the active listings in each category and return the count

- GET */sad/report/revenue?months=1*
	- how much money did we make from listing the past X months

- GET */sad/report/new_customers?months=1*
	- How many users registered the past X months

- GET */sad/csr/advertisers*
	- get all advertisers

- GET */sad/csr/advertisers/1*
	- get all listings for that specific advertiser by id

- GET */sad/csr?search=*
	- search everything

- GET */sad/sports/1*
	- get listing by path and id (* not in specifications but should be implement anyways)

- POST */sad/admin/listing*
	- Add new listing
	- Test JSON. Assuming category sports/soccer exists and customer 1 exists
~~~
{"customer_id": 1, "start_date": "Jun 1 2015  1:33PM", "end_date": "Dec 25 2015  1:33PM" ,
  "path": ["sports","soccer"], "content": "New ad from POST method", "cost": 400,
  "type": "category-featured", "website": "web.com", "image": "img"}
~~~

- POST */sad/admin/category/add*
	- create a new category 
	- Test JSON. Assuming category sports exists
~~~
{"Path":["sports"] , "SubCat_Name": "test"}
~~~

- PUT */sad/admin/category/edit*
	- edit category name
	- Test JSON. Assuming category sports exists
~~~
{"path": ["sports"], "new_name": "athletics"}
~~~

- DELETE */sad/admin/category/remove*
	- remove category if nothing exists
	- Test JSON. Assuming category sports/test exists
	- If it doesnt work check to see if you ran the previous path edit name and see if sports is not now athletics
~~~
{"path": ["sports","test"]}
~~~

- PUT */sad/admin/sort_setting*
	- edit sort setting name valid input ["random", "most-profitable", "most-recent", "advertiser-name"]
	- Test JSON 
~~~
{"setting":"most-profitable"}
~~~

- POST */sad/admin/advertiser*
	-  add a customer and get returned a customer id
	-  Test JSON. Assuming customer 
~~~
{"name": "New Name", "business_name": "new bus name",
 "registration_date": "Jun 1 2015  1:33PM", "phone": "1234", "fb": "myfb.com",
 "twitter": "mytwit.com", "linkedin": "newlink.com",
 "email": "new@gmail.com"}
~~~

- PUT */sad/admin/advertiser*
	-  edit an existing customer and get returned the contents of that advertiser
	-  Test JSON. Assuming customer 
~~~
{"customer_id": 1, "name": "New Name", "business_name": "new bus name",
 "registration_date": "Jun 1 2015  1:33PM", "phone": "1234", "fb": "myfb.com",
 "twitter": "mytwit.com", "linkedin": "newlink.com",
 "email": "new@gmail.com"}
~~~




#Technical Specs#
Persistence: Python Shelve

REST Framework: Flask

Test: Python unittest, py.tests