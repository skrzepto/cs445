from src.Delivery.REST import rest_sad
import pytest

"""
Do to time constraints and variations of the results i will only test the rest status

"""


@pytest.fixture
def client(request):
    rest_sad.init_data('test', False)
    return rest_sad.app.test_client()


def test_hello(client):
    # get the hello message
    r = client.get('/')
    assert r.status == '200 OK'


# /sad

def test_getsadhome(client):
    # get the home page
    r = client.get('/sad')
    assert r.status == '200 OK'


# /sad/report/count_active_listings

def test_report_active_listings(client):
    r = client.get('/sad/report/count_active_listings')
    assert r.status == '200 OK'


# /sad/report/revenue

def test_report_revenue_valid(client):
    r = client.get('/sad/report/revenue?months=1')
    assert r.status == '200 OK'


def test_report_revenue_notvalidargs(client):
    r = client.get('/sad/report/revenue?garbage=1')
    assert r.status == '400 BAD REQUEST'


def test_report_revenue_notvalid_value(client):
    r = client.get('/sad/report/revenue?months=a')
    assert r.status == '400 BAD REQUEST'


# /sad/report/new_customers

def test_report_newcustomers(client):
    r = client.get('/sad/report/new_customers?months=1')
    assert r.status == '200 OK'


def test_report_newcustomers_invalidargs(client):
    r = client.get('/sad/report/new_customers?garbage=1')
    assert r.status == '400 BAD REQUEST'


def test_report_newcustomers_invalidvalue(client):
    r = client.get('/sad/report/new_customers?months=a')
    assert r.status == '400 BAD REQUEST'


# /sad/csr/advertisers

def test_get_advertisers(client):
    r = client.get('/sad/csr/advertisers')
    assert r.status == '200 OK'


def tst_get_advertiser_byid_valid(client):
    r = client.get('/sad/csr/advertisers/1')
    assert r.status == '200 OK'


def tst_get_advertiser_byid_notfound(client):
    r = client.get('/sad/csr/advertisers/9999')
    assert r.status == '404 NOT FOUND'


# /sad/csr
def test_csr_search(client):
    r = client.get('/sad/csr?search=waldo')
    assert r.status == '200 OK'


def test_csr_search_invalid_args(client):
    r = client.get('/sad/csr?garbage=waldo')
    assert r.status == '400 BAD REQUEST'


# /sad/path/<int>
def test_getinvalidcategory(client):
    r = client.get('/sad/nothere')
    assert r.status == '404 NOT FOUND'


def test_getlistingbyid(client):
    r = client.get('/sad/sports/1')
    assert r.status == '200 OK'


def test_getcategory_valid(client):
    r = client.get('/sad/sports')
    assert r.status == '200 OK'


