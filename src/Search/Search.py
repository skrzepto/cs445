class Search:

    @staticmethod
    def _search(original_str, key_str):
        if original_str.lower().find(key_str.lower()) >= 0:
            return True
        else:
            return False
