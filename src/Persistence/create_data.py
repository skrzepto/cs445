from src.Customer.AllCustomers import AllCustomers
from src.sad import SAD
from src.Listing.Listing import Listing
from datetime import datetime
import dateutil.relativedelta

class create_data:
    def __init__(self):
        self.sad = SAD()

    def create3lvl_with_fake_customers(self):
            d_now = datetime.now()
            d_lastweek = d_now - dateutil.relativedelta.relativedelta(days=7)
            d_lastmonth = d_now - dateutil.relativedelta.relativedelta(months=1)
            d_last8months = d_now - dateutil.relativedelta.relativedelta(months=8)
            d_last5months = d_now - dateutil.relativedelta.relativedelta(months=5)

            st = [(1, u'New name', u'New email2', d_last5months.strftime('%b %d %Y %I:%M%p')),
                  (2, u'test name2', u'test2@gmail.com', d_lastmonth.strftime('%b %d %Y %I:%M%p')),
                  (3, u'test name3', u'test3@gmail.com', d_last8months.strftime('%b %d %Y %I:%M%p'))]
            allcust = AllCustomers(st)

            self.sad.customers = allcust

            d_yesterday = d_now - dateutil.relativedelta.relativedelta(days=1)
            d_last2months = d_now - dateutil.relativedelta.relativedelta(months=2)
            d_last5months = d_now - dateutil.relativedelta.relativedelta(months=5)

            d_tommorow = d_now + dateutil.relativedelta.relativedelta(days=1)
            d_nextweek = d_now + dateutil.relativedelta.relativedelta(days=7)
            d_nextmonth = d_now + dateutil.relativedelta.relativedelta(months=1)

            l_home = Listing(1, 1)
            l_home.updateType('home-page-featured')
            l_home.updateContent('This is a home page listing')
            l_home.setCost(2500)
            l_home.set_init_date(d_yesterday)
            l_home.setEndDate(d_nextmonth)
            l_home.setPath([])
            self.sad.home_cat.addListing(l_home)

            l = Listing(2)
            l.set_init_date(d_lastmonth)
            l.setEndDate(d_nextweek)
            l.updateContent('where is waldo in sports: normal')
            l.setPath(['sports'])
            l.setCost(300)

            l_1 = Listing(1)
            l_1.set_init_date(d_last5months)
            l_1.setEndDate(d_nextmonth)
            l_1.updateContent('where is waldo in sports: category featured')
            l_1.updateType('category-featured')
            l_1.setPath(['sports'])
            l_1.setCost(400)

            cat_sports = self.sad.home_cat.addSubCategory('sports')
            cat_sports['sports'].addListing(l)
            cat_sports['sports'].addListing(l_1)

            l2 = Listing(3)
            l2.set_init_date(d_last2months)
            l2.setEndDate(d_nextmonth)
            l2.setCost(250)
            l2.setPath(['sports', 'soccer'])
            l2.updateContent('waldo is in sports featured')

            cat_soccer = cat_sports['sports'].addSubCategory('soccer')
            cat_soccer['soccer'].addListing(l2)

            l3 = Listing(2)
            l3.set_init_date(d_lastweek)
            l3.setEndDate(d_nextmonth)
            l3.updateType('category-featured')
            l3.setPath(['sports', 'soccer', 'gear'])
            l3.updateContent('waldo is in sports featured')

            cat_gear = cat_soccer['soccer'].addSubCategory('gear')
            cat_gear['gear'].addListing(l3)
            emp = cat_gear['gear'].addSubCategory('empty')

            return self.sad
