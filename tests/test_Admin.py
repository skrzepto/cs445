import unittest2 as unittest
from src.Actors.Admin import Admin
from src.Customer.AllCustomers import AllCustomers
from src.sad import SAD
from src.Listing.Listing import Listing
from datetime import datetime
import dateutil.relativedelta
from src.Customer.Customer import Customer


class testAdmin(unittest.TestCase):
    def setUp(self):
        self.sad = SAD()
        self.create3lvl_with_fake_customers()
        self.admin = Admin(self.sad)

    def test_addCategory(self):
        self.assertIsNotNone(self.admin.addSubCategory(['sports'], 'basketball'))

    def test_addCategory_notvalid_2(self):
        self.assertIsNone(self.admin.addSubCategory(['none'], 'basketball'))

    def test_addCategory_notvalid_3(self):
        self.assertIsNone(self.admin.addSubCategory(['sports'], 'admin'))

    def test_changename_valid(self):
        self.assertEqual(self.admin.editnameSubCategory(['sports'], 'athletics'), 'athletics')

    def test_changename_valid_check_persistence(self):
        self.admin.editnameSubCategory(['sports'], 'athletics')

        all_sub = self.sad.home_cat.getAllSubCategories()
        print(all_sub)

        self.assertTrue('athletics' in all_sub[0])

    def test_changename_notvalid(self):
        self.assertIsNone(self.admin.editnameSubCategory(['none'], 'athletics'))

    def test_deleteSubCategory_valid(self):
        tmp = self.admin.deleteSubCategory(['sports', 'soccer', 'gear', 'empty'])
        self.assertTrue(tmp, 'Succesfully delete empty category from gear')

    def test_deleteSubCategory_notvalid(self):
        tmp = self.admin.deleteSubCategory(['sports', 'soccer', 'gear'])
        self.assertIsNone(tmp)

    def test_addCustomer_valid(self):
        cust = Customer('some_name', 'email@test.com', 0)
        res = self.admin.addCustomer(cust)
        print(res.get_custId())
        self.assertEqual(res, cust)

    def test_addListing(self):
        d_now = datetime.now()
        d_nextweek = d_now + dateutil.relativedelta.relativedelta(days=7)
        d_lastmonth = d_now - dateutil.relativedelta.relativedelta(months=1)

        l = Listing(1)
        l.set_init_date(d_lastmonth)
        l.setEndDate(d_nextweek)
        l.updateContent('testing add listing by admin')
        l.setCost(300)
        path = ['sports']
        l.setPath(path)
        self.assertEqual(self.admin.addListing(l), l)

    def test_addListing_nothappy(self):
        d_now = datetime.now()
        d_nextweek = d_now + dateutil.relativedelta.relativedelta(days=7)
        d_lastmonth = d_now - dateutil.relativedelta.relativedelta(months=1)

        l = Listing(0)
        l.set_init_date(d_lastmonth)
        l.setEndDate(d_nextweek)
        l.updateContent('testing add listing by admin')
        l.setCost(300)
        path = ['notvalid']
        l.setPath(path)
        self.assertIsNone(self.admin.addListing(l))

    def test_addListing_homepage_valid(self):
        d_now = datetime.now()
        d_nextweek = d_now + dateutil.relativedelta.relativedelta(days=7)
        d_lastmonth = d_now - dateutil.relativedelta.relativedelta(months=1)

        l = Listing(2)
        l.set_init_date(d_lastmonth)
        l.setEndDate(d_nextweek)
        l.updateContent('testing add listing by admin')
        l.setCost(300)
        l.updateType('home-page-featured')
        path = []
        l.setPath(path)
        self.assertEqual(self.admin.addListing(l), l)

    def test_addListing_homepage_notvalid(self):
        d_now = datetime.now()
        d_nextweek = d_now + dateutil.relativedelta.relativedelta(days=7)
        d_lastmonth = d_now - dateutil.relativedelta.relativedelta(months=1)

        l = Listing(0)
        l.set_init_date(d_lastmonth)
        l.setEndDate(d_nextweek)
        l.updateContent('testing add listing by admin')
        l.setCost(300)
        path = []
        self.assertIsNone(self.admin.addListing( l))

    def test_editSortSetting_valid(self):
        self.assertEqual(self.admin.editSortSetting('random'), 'random')

    def test_editsortsetting_notvalid(self):
        self.assertIsNone(self.admin.editSortSetting('garbage'))

    def test_getcustomer_valid(self):
        self.assertIsNotNone(self.admin.getCustomer(1))

    def test_getCusomter_none(self):
        self.assertIsNone(self.admin.getCustomer(99))


    def create3lvl_with_fake_customers(self):
        d_now = datetime.now()
        d_lastweek = d_now - dateutil.relativedelta.relativedelta(days=7)
        d_lastmonth = d_now - dateutil.relativedelta.relativedelta(months=1)
        d_last8months = d_now - dateutil.relativedelta.relativedelta(months=8)
        d_last5months = d_now - dateutil.relativedelta.relativedelta(months=5)

        st = [(1, u'New name', u'New email2', d_last5months.strftime('%b %d %Y %I:%M%p')),
              (2, u'test name2', u'test2@gmail.com', d_lastmonth.strftime('%b %d %Y %I:%M%p')),
              (3, u'test name3', u'test3@gmail.com', d_last8months.strftime('%b %d %Y %I:%M%p'))]
        allcust = AllCustomers(st)

        self.sad.customers = allcust

        d_yesterday = d_now - dateutil.relativedelta.relativedelta(days=1)
        d_last2months = d_now - dateutil.relativedelta.relativedelta(months=2)
        d_last5months = d_now - dateutil.relativedelta.relativedelta(months=5)

        d_tommorow = d_now + dateutil.relativedelta.relativedelta(days=1)
        d_nextweek = d_now + dateutil.relativedelta.relativedelta(days=7)
        d_nextmonth = d_now + dateutil.relativedelta.relativedelta(months=1)

        l_home = Listing(0, 1)
        l_home.updateType('home-page-featured')
        l_home.updateContent('This is a home page listing')
        l_home.setCost(2500)
        l_home.set_init_date(d_yesterday)
        l_home.setEndDate(d_nextmonth)
        self.sad.home_cat.addListing(l_home)

        l = Listing(0)
        l.set_init_date(d_lastmonth)
        l.setEndDate(d_nextweek)
        l.updateContent('where is waldo in sports: normal')
        l.setCost(300)

        l_1 = Listing(0)
        l_1.set_init_date(d_last5months)
        l_1.setEndDate(d_last2months)
        l_1.updateContent('where is waldo in sports: category featured')
        l_1.updateType('category-featured')
        l_1.setCost(400)

        cat_sports = self.sad.home_cat.addSubCategory('sports')
        cat_sports['sports'].addListing(l)
        cat_sports['sports'].addListing(l_1)

        l2 = Listing(0)
        l2.set_init_date(d_last2months)
        l2.setEndDate(d_nextmonth)
        l2.setCost(250)
        l2.updateContent('waldo is in sports featured')

        cat_soccer = cat_sports['sports'].addSubCategory('soccer')
        cat_soccer['soccer'].addListing(l2)

        l3 = Listing(0)
        l3.set_init_date(d_lastweek)
        l3.setEndDate(d_nextmonth)
        l3.updateType('category-featured')
        l3.updateContent('waldo is in sports featured')

        cat_gear = cat_soccer['soccer'].addSubCategory('gear')
        cat_gear['gear'].addListing(l3)
        emp = cat_gear['gear'].addSubCategory('empty')

        print(self.sad.home_cat)


if __name__ == '__main__':
    unittest.main()
