

class Admin:
    def __init__(self, sad):
        self.home = sad.home_cat
        self.sad = sad
        self.cust = self.sad.customers

    def addSubCategory(self, path, sub_cat_name):
        sub_cat = self.home.traverse(path)
        if sub_cat:
            if sub_cat_name not in ['admin', 'csr', 'report']:
                res = sub_cat.addSubCategory(str(sub_cat_name))
                return res
        return None

    def editnameSubCategory(self, path, new_name):
        sub_cat = self.sad.home_cat.traverse(path)
        if sub_cat:
            old_name = sub_cat.getName()
            sub_cat.setName(str(new_name))
            s_cat = self.sad.home_cat.traverse(path[:-1])

            for d in s_cat.getAllSubCategories():
                for keys in d:
                    if  keys == old_name:
                        d[new_name] = d[old_name]
                        del d[old_name]
                        return new_name
        return None

    def deleteSubCategory(self, path):
        #sub_cat = self.home.home_cat
        # you must delete a category at a time with no branches under it.
        # I have it set where you can not wipe the entire tree
        sub_cat = self.home.traverse(path[0:-1])
        if sub_cat:
            if sub_cat.removeSubCategory(str(path[-1])):
                return True

        return None

    def addCustomer(self, cust):
        return self.cust.addCustomer(cust)

    #def updateCustomer(self, cust):
    #   pass

    def addListing(self, new_l):
        if new_l:
            sub_cat = self.sad.traverse(new_l.getPath())
            if sub_cat and self.sad.getAdvertiserbyID(new_l.getCustID()):
                # check if path exists
                # check if customer exists
                res = sub_cat.addListing(new_l)  # add to appropriate category
                return res
        return None

    def getCustomer(self, c_id):
        return self.sad.getAdvertiserbyID(c_id)

    def editSortSetting(self, setting):
        return self.sad.editSortSetting(setting)